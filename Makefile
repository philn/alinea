all: test

beautiful:
	./tools/beautiful.sh

clean:
	find . -name "*.pyc" -exec rm {} \;
	find . -name "*~" -exec rm {} \;
	find . -name "*.tmp" -exec rm {} \;
	find . -name "*.tmp~" -exec rm {} \;

test: clean
	python ./server_medusa.py
