from ez_setup import use_setuptools
use_setuptools()
from setuptools import setup, find_packages
from Alinea import __version__ as alineaVersion
from Alinea.packages import packages as alineaPackages

import sys
# patch distutils if it can't cope with the "classifiers" keyword
if sys.version < '2.2.3':
    from distutils.dist import DistributionMetadata
    DistributionMetadata.classifiers = None
    DistributionMetadata.download_url = None

# build the packages list
packages = ["Alinea",] + [ "Alinea.%s" % package
                           for package in find_packages("Alinea") ]

# build the templates list, depending on the alinea packages
dataFiles = ['templates/*.tmpl']
map(lambda x: dataFiles.extend(list(x)), [('templates/%s/*.tmpl' % name,
                                           'templates/%s/admin/*.tmpl' % name)
                                          for name in alineaPackages ])
if 'Articles' in alineaPackages:
    dataFiles.append('templates/Articles/archives/*.tmpl')


setup(name="Alinea",
      version=alineaVersion,
      description="Yet another blog framework",
      long_description="""\
Alinea is a blog tool designed to be modular at some extent. Basically
it manages some users who write some articles organized in
sections. Readers can react to articles by posting comments.
""",
      classifiers=["Development Status :: 4 - Beta",
                   "Environment :: Web Environment",
                   "Natural Language :: English",
                   "Operating System :: OS Independent",
                   "Topic :: Internet :: WWW/HTTP",
                   "Topic :: Internet :: WWW/HTTP :: Dynamic Content :: News/Diary",
                   "Topic :: Internet :: WWW/HTTP :: Site Management",                   
                   "License :: OSI Approved :: GNU General Public License (GPL)",
                   "Programming Language :: Python",
                   ],
      author="PythonFR team",
      author_email="contact@pythonfr.org",
      url="http://pythonfr.org/alinea/",
      license="GPL",
      packages=packages,
      package_data={'Alinea': dataFiles},
      scripts=["scripts/alinea-run"],
      install_requires=["SQLObject>=0.6.1", "Quixote>=2.0", "Cheetah>=0.9.17"],
      extras_require = {
    'ReST': ["docutils>=0.3.7",],
    'SilverCity': ["docutils>=0.3.7", "SilverCity>=0.9.7"],
    'SCGI': ["scgi>=1.7"],
    }
      )
