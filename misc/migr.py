"""
Use this to migrate db from Alinea2 to Alinea3.

1. The Alinea3 db shall firstly be created using alinea-run --install
2. Find and edit db connection uris below to suit your needs
3. run me, sweet God !
4. Hopefully delete your alinea2 db, for goodness sake :)

"""

from sqlobject import *
from sqlobject.inheritance import InheritableSQLObject
import datetime

from Alinea.AlineaLib.AlineaNode import AlineaNode
from Alinea.Sections.AlineaSection import AlineaSection
from Alinea.Articles.AlineaArticle import AlineaArticle
from Alinea.Comments.AlineaComment import AlineaComment

############################### Old DB Schema

class OldAlineaUser(SQLObject):
    _table = 'alinea_user'
    firstName = StringCol(length=128)
    lastName  = StringCol(length=128)
    email     = StringCol(length=128,default='')
    login     = StringCol(length=128,alternateID=True)
    passwd    = StringCol(length=128)
    session   = StringCol(length=64,default='')

class OldAlineaSection(SQLObject):
    _table = 'alinea_section'
    name           = StringCol(length = 128)
    alineaArticles = MultipleJoin('OldAlineaArticle',orderBy="date")
    parent         = ForeignKey('OldAlineaSection')

class OldAlineaArticle(SQLObject):
    _table = 'alinea_article'
    title     = StringCol(length = 100)
    format    = StringCol(length = 64)
    data      = StringCol()
    html      = StringCol()
    date      = DateTimeCol(default=datetime.datetime.now)
    published = BoolCol(default= False)
    alineaSection = ForeignKey('OldAlineaSection')
    alineaUser = ForeignKey('OldAlineaUser')
    comments  = MultipleJoin('OldAlineaComment')

class OldAlineaComment(SQLObject):
    _table = 'alinea_comment'
    author         = StringCol(length = 128)
    email          = StringCol(length=128)
    url            = StringCol(length=128)
    ip             = StringCol(length=64)
    comment        = StringCol()
    date           = DateTimeCol()
    alinea_article = ForeignKey('OldAlineaArticle')

def tableCopy(table, oldConn, newConn):
    print 'Copying %s' % table.__name__
    table._connection = newConn
    
    rowsNb = 0
    # dump the old table to the new one
    for row in table.select(connection=oldConn):
        colsDict = {'id': row.id}
        for col in table._columns:
            colsDict[col.name] = getattr(row,col.name)
        newRow = table(**colsDict)
        rowsNb += 1
    print "  Copied %s rows" % rowsNb



conn1 = connectionForURI('postgres://phil:phil@localhost/ludo_alinea2')
conn2 = connectionForURI('postgres://phil:phil@localhost/ludo_alinea3')

tableCopy(OldAlineaUser, conn1, conn2)


OldAlineaSection._connection = conn1
AlineaSection._connection = conn2

OldAlineaArticle._connection = conn1
AlineaArticle._connection = conn2

OldAlineaComment._connection = conn1
AlineaComment._connection = conn2

AlineaNode._connection = conn2

rootSection = AlineaSection.select(AlineaSection.q.name=='Alinea')[0]

def migrateSections(sections):
    
    for section in sections:
        parent = section.parent
        if not parent:
            newParent = rootSection
        else:
            newParent = AlineaSection.select(AlineaSection.q.publicID == parent.id)[0]

        newSection = AlineaSection(name=section.name)
        rgt = AlineaNode.openHole(newParent)
        newSection.set(lft=rgt, rgt=rgt+1)
        newSection.publicID = section.id
        AlineaNode.clearCache()

        print '%s - %s' % (newSection.publicID,newSection.name)

        # Articles of section
        for article in section.alineaArticles:
            print '   - %s : %s' % (article.id, article.title)

            newArticle = AlineaArticle(title=article.title, data=article.data,
                                       format=article.format, date=article.date,
                                       lastModified=article.date,
                                       hits=0, published=article.published,
                                       html=article.html,alineaUser=article.alineaUser)
            rgt = AlineaNode.openHole(newSection)
            newArticle.set(lft=rgt, rgt=rgt+1, publicID=article.id)
            AlineaNode.clearCache()
            assert newArticle.publicID == article.id

            # Comments of the article
            for comment in article.comments:
                newComment = AlineaComment(author=comment.author, email=comment.email,
                                           url=comment.url, ip=comment.ip,
                                           comment=comment.comment,date=comment.date)

                rgt = AlineaNode.openHole(newArticle)
                newComment.set(lft=rgt, rgt=rgt+1, publicID=comment.id)
                AlineaNode.clearCache()
                print '      - %s | %s' % (newComment.publicID, newComment.comment[:20])                
                
migrateSections(OldAlineaSection.select(OldAlineaSection.q.parentID == None, orderBy="id"))
migrateSections(OldAlineaSection.select(OldAlineaSection.q.parentID != None, orderBy="id"))
