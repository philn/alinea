# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import sys
from sqlobject import dbconnection
from sqlobject import *

# Alinea specific
from Alinea.AlineaLib.AlineaNode import AlineaNode
from Alinea.Articles.AlineaArticle import AlineaArticle
from Alinea.Users.AlineaUser import AlineaUser, AlineaRole
from Alinea.Sections.AlineaSection import AlineaSection
from Alinea.Comments.AlineaComment import AlineaComment
tables = [ AlineaNode, AlineaRole, AlineaUser,
           AlineaSection, AlineaArticle,  AlineaComment ]


def registerConnection(tables, conn):
    for table in tables:
        #print 'Registering [%s] connection to table [%s]' % (conn.uri(),table.__name__)
        table.setConnection(conn)
        globals()[table.__name__] = table

def resetTables(tables):
    for table in tables:
        print 'Resetting ...', table.__name__
        table.dropTable(ifExists=True, cascade=True)
        table.createTable()


def tableCopy(table, oldConn, newConn):
    # dump the old table to the new one
    for row in table.select(connection=oldConn):
        colsDict = {'id': row.id}
        for col in table._columns:
            colsDict[col.name] = getattr(row,col.name)
        newRow = table(**colsDict)    

def dbCopy(oldConn, newConn):
    # register the new connection to the tables
    registerConnection(tables, newConn)
    # reset the tables of the new db
    resetTables(tables)

    # walk on the tables of the old db
    for table in tables:
        tableCopy(table, oldConn, newConn)

def usage():
    print """\
Usage: %s old_db_uri new_db_uri

- old_db_uri is a SQLObject db uri identifying the db to copy
  example: sqlite:///tmp/myOld.db
- new_db_uri is a SQLObject db uri identifying the db to create
  example: mysql://user:password@localhost/myNewDB
""" % sys.argv[0]
    sys.exit(1)
    

def main():
    if len(sys.argv[1:]) != 2:
        usage()
    oldConn = dbconnection.connectionForURI(sys.argv[1])
    newConn = dbconnection.connectionForURI(sys.argv[2])
    dbCopy(oldConn, newConn)

if __name__ == '__main__':
    main()
