ALTER TABLE alinea_article add column "language" text;          
ALTER TABLE alinea_article alter "language" set default 'en';
update alinea_article set "language" = 'en';

