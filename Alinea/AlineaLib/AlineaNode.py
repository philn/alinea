# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import re
from sqlobject import *
from sqlobject import sqlbuilder
from sqlobject.inheritance import InheritableSQLObject
from Alinea.config import site_admin_url, site_url, db_uri
from Alinea.AlineaLib.AlineaTools import getObjectWithClassAndID
from Alinea.AlineaLib.DBConnection import DBConnection

from Alinea.AlineaLib.Hub import HUB

__all__ = ['AlineaNode', 'Alinea_getNodeWithID']

class AlineaNode(InheritableSQLObject):
    _connection = HUB
    lft = IntCol(default=0)
    rgt = IntCol(default=0)

    def doQuery(cls, query):
        " execute a query to the table using its associated connection "
        conn = cls._connection
        try:
            query = query.__sqlrepr__(conn)
            # i don't really like SQLObject logs format so i simply
            # dump the queries 'manually' forged (cf moveTo() for
            # instance)
            if conn.debug:
                print query
            conn.queryAll(query)
        except: pass

    doQuery = classmethod(doQuery)

    def clearCache(cls):
        """ Clear that damned SQLObject cache which tends to get
        out-dated when using sqlbuilder.Update. """
        cls._connection.cache.clear()
        
    clearCache = classmethod(clearCache)
        
    def assertValidTree(self):
        self.__class__.clearCache()
        nodes = AlineaNode.select(AlineaNode.q.lft >= AlineaNode.q.rgt)
	try:
	    nodes = list(nodes)
	except:
	    pass
	else:
	    assert len(nodes) == 0, "The tree is not valid."

    def delete(cls, id):
        obj = cls.get(id)
        
        table = AlineaNode
        children = table.select(AND(table.q.lft >= obj.lft,
                                    table.q.rgt <  obj.rgt))
        children = list(children)
        childrenNb = len(children)
        
        # drop children
        for child in children:
            child.destroySelf()

        # close the hole resulting of children deletion
        cls.closeHole(obj,childrenNb)

        # kill it, it's dead!
        obj.destroySelf()
        
        cls.clearCache()
        
    delete = classmethod(delete)

    def moveTo(self, newParent):
        " move the node to another one "
        cls = AlineaNode
        table = cls.sqlmeta.table

        # is the node moving some level up or down ?
        toTop = self.lft > newParent.lft

        parentID = newParent.id
        myID = self.id

        node = self
        nodeLft = self.lft
        nodeRgt = self.rgt
        nodeChildren = cls.select(AND(cls.q.lft >= nodeLft,
                                      cls.q.rgt <  nodeRgt))
        nodeChildren = list(nodeChildren)
        nodeChildrenNb = len(nodeChildren)

        nodeParent = node.getParent()
        nodeParentLft = nodeParent.lft
        nodeParentRgt = nodeParent.rgt

        oldParentRgt = newParent.rgt
        oldParentLft = newParent.lft

        """
        XXX: this is buggy but once debugged will be faster than current siblings
             method fetching (see below)
             
        clsAlias1 = sqlbuilder.Alias(cls)
        clsAlias2 = sqlbuilder.Alias(cls)
        
        childrenCond = AND(clsAlias2.q.lft > clsAlias1.q.lft,
                           clsAlias2.q.rgt < clsAlias1.q.rgt)

        noChild = sqlbuilder.NOTEXISTS(sqlbuilder.Select(where=childrenCond))
        siblings = cls.select(AND(clsAlias1.q.lft >= oldParentLft,
                                  clsAlias1.q.rgt <  oldParentRgt,
                                  noChild),
                              orderBy='lft')
        siblings = list(siblings)                              
        """

        # XXX: THIS IS TOO SLOW !!!
        siblings = cls.select(AND(cls.q.lft >= oldParentLft,
                                  cls.q.rgt <  oldParentRgt),
                              orderBy='lft')
        siblings = list(siblings)            
        siblings = [ s for s in siblings if s.getParent() == newParent ]
        #
        
        siblingsNb = len(siblings)
        print '%s siblings ...' % siblingsNb
        
        # record node children positions
        memory = {}
        for child in nodeChildren:
            memory[child.id] = (child.lft - nodeLft,
                                child.rgt - nodeLft)

        # mark the node children (to prevent them being incidentally 'moved')
        if nodeChildrenNb:
            cls.doQuery(sqlbuilder.Update(table, {'rgt': - cls.q.rgt,
                                                  'lft': - cls.q.lft},
                                          where=AND(cls.q.lft > nodeLft,
                                                    cls.q.lft < nodeRgt)))

        # update the nodes between the node to move and its future parent
        value = nodeChildrenNb * 2 + 2
        if toTop:
            cls.doQuery(sqlbuilder.Update(table, {'lft': cls.q.lft + value},
                                          where=AND(cls.q.lft > oldParentLft,
                                                    cls.q.lft < nodeLft)))
            if siblingsNb > 0:
                firstParentChildLft = siblings[0].lft
                cond = AND(cls.q.rgt >= firstParentChildLft,
                           cls.q.rgt <= (nodeLft-1))                
            else:
                cond = AND(cls.q.rgt < nodeRgt, cls.q.rgt > oldParentRgt)
            cls.doQuery(sqlbuilder.Update(table, {'rgt': cls.q.rgt + value},
                                          where=cond))
        else:
            cls.doQuery(sqlbuilder.Update(table, {'rgt': cls.q.rgt - value},
                                          where=AND(cls.q.rgt >= nodeRgt,
                                                    cls.q.rgt < oldParentLft)))
            
            cls.doQuery(sqlbuilder.Update(table, {'lft': cls.q.lft - value},
                                          where=AND(cls.q.lft > nodeLft,
                                                    cls.q.lft <= oldParentLft)))

        if toTop:
            parentLft = newParent.lft
        else:
            parentLft = newParent.lft - ((nodeChildrenNb + 1) * 2)
        newRgt = (nodeChildrenNb * 2) + parentLft + 2
        parentRgt = newRgt + (siblingsNb * 2) + 1
        newLft = parentLft + 1
        cls.doQuery(sqlbuilder.Update(table, {'lft': newLft,
                                              'rgt': newRgt },
                                      where=cls.q.id == myID))

        cls.doQuery(sqlbuilder.Update(table, {'lft': parentLft},
                                      where=cls.q.id == parentID))
        
        if not toTop or siblingsNb == 0:
            cls.doQuery(sqlbuilder.Update(table, {'rgt': parentRgt },
                                      where=cls.q.id == parentID))

        # restore the node positions
        for nid, infos in memory.iteritems():
            lft, rgt = infos
            cls.doQuery(sqlbuilder.Update(table, {'rgt': newLft + rgt ,
                                                  'lft': newLft + lft },
                                          where=cls.q.id == nid))

        self.assertValidTree()

    def openHole(cls, node):
        """ Create a hole in the lft/rgt structure.

            This is useful when you want to insert a new Node under the
            node passed as parameter.
        """
        rgt = node.rgt
        table = cls.sqlmeta.table

        cls.doQuery(sqlbuilder.Update(table, {'rgt': cls.q.rgt + 2},
                                      where=cls.q.rgt >= rgt))        
        cls.doQuery(sqlbuilder.Update(table, {'lft': cls.q.lft + 2},
                                      where=cls.q.lft >= rgt))
        return rgt

    openHole = classmethod(openHole)

    def closeHole(cls, node, siblingsNb):
        """ Modify the lft/rgt structure to put `node` away.
        """
        rgt = node.rgt
        lft = node.lft
        table = AlineaNode.sqlmeta.table
        
        value = siblingsNb * 2 + 2
        
        cls.doQuery(sqlbuilder.Update(table, {'rgt': cls.q.rgt - value},
                                      where=cls.q.rgt > rgt))
        cls.doQuery(sqlbuilder.Update(table, {'lft': cls.q.lft - value},
                                      where=cls.q.lft > lft))
        
    closeHole = classmethod(closeHole)

    def path(self):
        """ path to the node as an ordered list of AlineaNodes
            XXX: probably needs some caching since it's used by depth()
                 and getParent()
        """
        parents = AlineaNode.select(AND(AlineaNode.q.rgt > self.rgt,
                                        AlineaNode.q.lft < self.lft),
                                    orderBy=AlineaNode.q.lft)
        try:
            result = list(parents)
        except IndexError:
            result = []
        return result

    def depth(self):
        " length of the node's path "
        return len(self.path())

    def getParent(self):
        " direct parent of the node "
        parents = self.path()
        try:
            parent = parents[-1]
        except IndexError:
            # can this be ?
            parent = None
        return parent
    
    def getChildren(self, table=None):
        " all children of the node, optionally filtered by table name "
        if table is None:
            table = self.__class__
        rows = table.select(AND(table.q.lft >= self.lft,
                                table.q.rgt <  self.rgt),
                            orderBy='lft', reversed=False)
        # we don't convert the select results to a list so that
        # it's still possible to post-process them (filter or add another condition)
        return rows

    def getChildrenNb(self):
        " number of children of the node "
        rgt, lft = self.rgt, self.lft
        assert rgt > lft
        return (rgt - lft - 1) / 2

    def getTypeAsString(self):
	#raise NotImplemented
	return ""

    def getHtml(self):
        return ''

    def getEncodedHtml(self):
        html = re.sub('&','&amp;', self.getHtml())
        html = re.sub('<','&lt;', html)
        html = re.sub('>','&gt;', html)
        return html

    def title_or_id(self):
        return str(self.id)

    def public_url(self):
        """ return the full url for a Node """
        raise NotImplemented

    def admin_url(self):
        """ return the Node URL in admin site view """
        raise NotImplemented
    
AlineaNode.sqlmeta.cacheValues = True

def Alinea_getNodeWithID(id):
    obj = getObjectWithClassAndID(AlineaNode,id)
    return obj
