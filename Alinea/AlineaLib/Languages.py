
import gettext
import os

class Languages:

    def __init__(self):
        self._langs = {}
        self.load()

    def __getitem__(self, lang):
        return self._langs[lang]

    def load(self):
        # XXX: hum .mo file shouldn't be in Alinea/locale
        #      but in some place like /usr/share/locale/
        alinea = __import__('Alinea')
        localeDir = os.path.join(os.path.dirname(alinea.__file__), 'locale')
        dirItems = os.listdir(localeDir)
        for dirItem in dirItems:
            name = os.path.join(localeDir, dirItem)
            if os.path.isdir(name) and dirItem != '.svn':
                try:
                    self._langs[dirItem] = gettext.translation('alinea', localeDir, [dirItem])
                except Exception, e:
                    print 'Error: Loading language: "%s"' % dirItem
                    print e

    def availableLanguages(self):
        return self._langs.keys()
                
    def gettextFunc(self, lang):
        if self._langs.has_key(lang):
            return self._langs[lang].gettext
        else:
            return lambda x: x
