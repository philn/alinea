# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$


"""

This handy module provides few nice things:

- a code-block directive (if SilverCity_ installed)
- a quote-block directive
- a custom `Publisher` which:
  * is thread safe (need some testings)
  * publish HTML snippets (understand 'without nasty headers and tags')
  * remembers ReST compilation warnings

Simply importing this module will make chosen directives available
in ReST core.

`Publisher` is quite easy of use, see auto-test at the end of this
file. Few parts of the publisher are taken from an ASPN Recipe.

** Code under GPL **

.. _SilerCity: http://silvercity.sf.net

"""

try:
    import SilverCity
    from SilverCity import LanguageInfo
    import urlparse,urllib2
except ImportError:
    SilverCity = None
    print 'SilverCity package not found..'
    print 'ReST code-block directive is disabled.'

import StringIO,string, threading
from docutils import core, io
import docutils.parsers.rst
from docutils import nodes, parsers
from docutils.writers.html4css1 import Writer, HTMLTranslator
from docutils.readers.standalone import Reader
from docutils.parsers.rst import Parser

def string_argument(argument):
    if argument is None:
        raise ValueError('argument required but none supplied')
    else:
        return ''.join([s.strip() for s in argument.splitlines()])

def beautify(list):
    """
    [u'toto',u'',u'',u'',u'tutu',u'']
       -->
            [u'toto',u'',u'tutu',u'']
    """
    i, new = 0, []
    while i < len(list):
        j = i+1
        if j < len(list):
            if list[i] == u'':
                new.append(list[j])
            elif list[j] == u'':
                new.append(list[i])
            i += 2
        else:
            new.append(list[i])
            i += 1
    return new

def code_block(name, arguments, options, content, lineno,
               content_offset, block_text, state, state_machine):
    """
    The code-block directive provides syntax highlighting for blocks
    of code.  It is used with the the following syntax:

    .. code-block::
       :lang: CPP

       #include <iostream>

       int main( int argc, char* argv[] ) {
          std::cout << "Hello world" << std::endl;
       }

    There is also an option called 'url'. It can be used to specify an URL
    or a local file to read an fill the block in with its contents.

    The directive requires the name of a language supported by SilverCity.
    All code in the indented block following
    the directive will be colourized. Note that this directive is only
    supported for HTML writers.
    """

    # Python language by default when lang option not set
    if options.has_key('lang'):
        lang = options['lang']
    else:
        lang = 'Python'

    # get the right HTML Silvercity generator
    try:
        module = getattr(SilverCity, lang)
        generator = getattr(module, lang+"HTMLGenerator")
    except AttributeError:
        block = nodes.literal_block(block_text, block_text)
        error = state_machine.reporter.error("No SilverCity lexer found"
                                             "for language '%s'." % lang,
                                             block, line=lineno)
        return [error]

    # use url option for filling content in
    content2 = None
    if options.has_key('url'):
        url = options['url']
        if (urlparse.urlparse(url)[0] == 'http'):
            # Get the file via http
            content2 = urllib2.urlopen(url).readlines()
            content2 = [ s.replace('\n','') for s in content2 ]
        else:
            # load local file
            content2 = [ s.replace('\n','') for s in open(url).readlines() ]

    if not content2:
        #content = beautify(content)
        pass
    else:
        content = content2

    # add line numbers
    i, highest, data = 0, len(str(len(content))),[]
    while i < len(content):
        lineNb = string.rjust(str(i+1),highest)
        data.append(u'%s %s' % (lineNb,content[i]))
        i += 1

    io = StringIO.StringIO()
    generator().generate_html(io, '\n'.join(data))
    html = '<div class="code-block">\n%s\n</div>\n' % io.getvalue()
    return [nodes.raw('',html, format = 'html')]

def quote_block(name, arguments, options, content, lineno,
                content_offset, block_text, state, state_machine):
    """ Quote directive. Example below:
        .. quote-block::
           :author: SoaF

           Example is better than a long talk.

    """
    auth = '<div class="quote-author">'+ options['author'] + '</div>'
    html = '<div class="quote-block">' + auth + '\n'.join(content) + '</div>'
    return [nodes.raw('',html, format = 'html')]


# activate code-block directive if we have SilverCity
if SilverCity:
    code_block.options = {'lang' : string_argument, 'url': string_argument}
    code_block.content = 1
    # Simply importing this module will make the directive available.
    parsers.rst.directives.register_directive('code-block', code_block)

# activate the quote-block directive
quote_block.content = 1
quote_block.options = {'author': string_argument }
parsers.rst.directives.register_directive('quote-block',quote_block)


class NoHeaderHTMLTranslator(HTMLTranslator):
    """ Header-less friendly and nice HTML Translator.
        (Thanks to an ASPN Recipe)
    """

    def __init__(self, document):
        HTMLTranslator.__init__(self, document)
        self.head = []
        self.head_prefix = []
        self.body_prefix = []
        self.body_suffix = []

    def astext(self):
        return ''.join(self.body)

    def visit_document(self, node):
        pass

    def depart_document(self, node):
        pass

class CustomStringInput(io.StringInput):
    """ Thread Safe (at least I hope :) StringInput
        implementation.
    """

    def __init__(self, source=None, source_path=None, encoding=None):
        io.StringInput.__init__(self,source,source_path,encoding)
        self.lock = threading.Lock()

    def read(self):
        """Read and decode a single file and return the data."""
        self.lock.acquire()
        ret = self.decode(self.source)
        self.lock.release()
        return ret

class CustomStringOutput(io.StringOutput):
    """ Thread Safe (still hoping !) StringOutput
        implementation
    """

    def __init__(self, source=None, source_path=None, encoding=None):
        io.StringOutput.__init__(self, source, source_path, encoding)
        self.lock = threading.Lock()

    def write(self, data):
        """Encode `data`, store it in `self.destination`, and return it."""
        self.lock.acquire()
        self.destination = self.encode(data)
        self.lock.release()
        return self.destination


class Publisher(core.Publisher):
    """
       This kind of publisher produces HTML snippets from
       reStructured code. It also keeps ReST warnings in its
       pocket (usefull to report errors on user-friendly apps :)
    """

    def __init__(self):

        core.Publisher.__init__(self)
        self.set_reader('standalone', None, 'restructuredtext')
        writer = Writer()

        # we don't want HTML headers in our writer
        writer.translator_class = NoHeaderHTMLTranslator
        self.writer = writer

        # go with the defaults
        self.get_settings()

        self.settings.xml_declaration = ""

        # this is needed, but doesn't seem to do anything
        self.settings._destination = ''

        # use no stylesheet
        self.settings.stylesheet = ''
        self.settings.stylesheet_path = ''
        self.settings.embed_stylesheet = False

        # don't break if we get errors
        self.settings.halt_level = 6

        # remember warnings
        self.warnings = ''

    def publish(self, data):
        """ Return the HTML formatted and the title of the
            reST block (can be None).
        """

        self.settings.warning_stream = StringIO.StringIO()

        # input
        self.source = CustomStringInput(source=data, encoding='iso-8859-1')

        # output - not that it's needed
        self.destination = CustomStringOutput(encoding='iso-8859-1')

        # parse!
        self.document = self.reader.read(self.source, self.parser, self.settings)

        # transform
        self.apply_transforms()

        self.warnings = ''.join(self.settings.warning_stream.getvalue())
        self.settings.warning_stream.close()

        # do the format
        return self.writer.write(self.document, self.destination)

    def getWarnings(self):
        return self.warnings

if __name__ == "__main__":
    data = """\
titre
=====

.. code-block::
  :url: ReST.py
"""
    #html = core.publish_string(data, reader_name='standalone', writer_name='html')
    #open('out.html','w').write(html)
    st = Publisher()
    open('out.html','w').write(st.publish(data))
