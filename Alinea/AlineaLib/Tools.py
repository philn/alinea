# Common Tools uset in servlets
# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$




import string,re


def beautyfullText(text):
    """ return a the text w/ some html cosemtics """
    temp = string.replace(text,'\n','<br />\n')
    temp = string.replace(temp,'  ','&nbsp; ')
    temp = re.sub('href=([_\=a-zA-Z\-0-9/:.&?]+)','href=\"\g<1>"',temp)
    temp = re.sub('src=([_\=a-zA-Z\-0-9/:.&?]+)','src=\"\g<1>"',temp)

    return temp

def unBeautyfullText(text):
    temp = string.replace(text,'<br/>','\n')
    temp = string.replace(temp,'&nbsp; ','  ')
    return temp


def bk():
    """
    Debugging: put a pdb breakpoint
    I'm too lazy, set_trace call is too long...
    """
    import pdb; pdb.set_trace()


from HTMLParser import HTMLParser

class CustomHTMLParser(HTMLParser):
    """ An HTML parser which strips HTML Tags :)
        Very usefull, huh ?
    """

    def __init__(self):
        HTMLParser.__init__(self)
        self.usefullData = ''

    def handle_data(self, data):
        if data != '' and '\n' not in data:
            self.usefullData += data

    def getUsefullData(self):
        return self.usefullData
