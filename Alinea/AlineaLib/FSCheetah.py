# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import string,time,os,sys, copy
from Cheetah.Compiler import Compiler
from Cheetah import Filters

def compileTemplate(fname, basename=None, name=None, verbose=False,
                    output_path=None, base_path=None):

    if not name:
        name = os.path.splitext(fname)[0]
    if not basename:
        basename = os.path.basename(name)

    # re-name & re-compile the default template if we're dealing
    # with a custom template
    if basename.endswith('_c'):
        basename = basename[:-2] # strip '_c' off
        name = name[:-2] # strip '_c' off
        
    elif not basename.endswith('_default'):
        compileTemplate(fname, basename+'_default', name+'_default',
                        output_path=output_path, base_path=base_path)

    name += ".py"

    if output_path and base_path:
        idx = name.find(base_path)
        end = len(base_path)
        dirName = name[idx+end:]
        if dirName.startswith(os.path.sep):
            dirName = dirName[1:]
        name = os.path.join(output_path, dirName)
    
    if verbose:
        print 'Compiling "%s" -> "%s"' % (fname, name)
        
    # let Cheetah compiler do some durty work                    
    obj = Compiler(file=fname,
                   moduleName=basename,
                   mainClassName=basename)

    try:
        f = open(name,'w')
    except IOError,err:
        print err
    else:
        f.write(str(obj))
        f.close()


# /!\ you need RELOAD for AUTO_COMPILE
FSCHEETAH_AUTO_RELOAD = True
FSCHEETAH_AUTO_COMPILE = True

class FSCheetahModuleError(Exception):pass

class FSCheetah:
    """ Cheetah file system template with automatic rebuild """

    def __init__(self,name):
        """ name == the full path of the template """
        self._name = name
        self._klassName = string.split(name,'.')[-1]
        self.load()

        self._instance = None

        # path manipulation
        self._abspath  = os.path.abspath(self._module.__file__)
        #self._abspath  = os.path.abspath(self._module.__CHEETAH_src__)
        self._dirname  = os.path.dirname(self._abspath)

        #self._py_path = self._module.__CHEETAH_src__
        self.set_template_name(self._klassName)

    def set_template_name(self, tmpl):
        """ set the template file depending on its class"""
        dirName = self._dirname
        path = os.path.join( dirName , '%s.py' % tmpl)
        if os.path.exists(path):
            self._klassName = tmpl
            oldName = self._name.split('.')[:-1]
            self._name = '.'.join(oldName + [tmpl])
            self._py_path = path
            self.load()
            return True
        return False
            
    def get_template_name(self):
        return self._klassName

    def get_module_name(self):
        return self._name

    def load(self):
        """ import the template module and create the instance """
        # we import the module w/ the full path
        try:
            self._module = __import__(self._name,[],[],self._klassName)
        except ImportError,msg:
	    print sys.path
	    raise
            msg = "%s:\n Unable to load '%s': check that you compiled it at least one time" % (msg,self._name)
            raise FSCheetahModuleError, msg
        
    def render(self):
        """ check if we need to reload .. and do it """
        # put self in the template namespace

        if FSCHEETAH_AUTO_RELOAD:
            if self.changed():
                if FSCHEETAH_AUTO_COMPILE:
                    self.build()
                reload(self._module)
                self._instance = None

        if not self._instance:
	    self._instance = getattr(self._module,self._klassName)(searchList=[self, ])
        data = self._instance.respond()
        return data

    def changed(self):
        """ return true if the filesystem template is newer """
        genTime = time.mktime( time.strptime( self._module.__CHEETAH_genTime__) )
        modTime = os.path.getmtime( self.tmpl_path() )
        return modTime > genTime

    def module(self):
        return self._module

    ### path manipulation ###
    def abspath(self):
        """ return the full name of a template """
        return self._abspath

    def dirname(self):
        """ return the folder where is the template """
        return self._dirname

    def py_path(self):
        """ return the source path for a template """
        return self._py_path

    def tmpl_path(self):
        return self._module.__CHEETAH_src__

    def src(self):
        f = open(self.tmpl_path())
        data = f.read()
        f.close()
        return data

    def build(self):
        output_path = os.path.dirname(self.py_path())
        base_path = os.path.dirname(self.tmpl_path())
        compileTemplate(self.tmpl_path(),verbose=True,output_path=output_path,
                        base_path=base_path)

    ### cache             ###
    def refreshCache(self, cacheRegionId, cacheId=None):
        if self._instance:
            cls = self._instance.__class__.__name__
            print 'Trying to refresh "%s.%s" on %s' % (cacheRegionId,cacheId, cls)
            try:
                self._instance.refreshCache(cacheRegionId,cacheId)
            except KeyError:
                # cache not found
                pass
            else:
                print 'Refreshed "%s.%s" on %s' % (cacheRegionId,cacheId, cls)
        else:
            print 'No Template instance on %s' % self.get_module_name()
