# Tools for AlineaObjects (Articles, Sections, ...)
# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$


from sqlobject import *

from Alinea.config import site_url,site_admin_url, db_uri

def getObjectWithClassAndID(klass,id):
    # how could this happens ???
    if id == None:
        return None
    try:
        return klass.get(id)
    except IndexError:
        return None


##########################################################
### Virtual Rows aka creating SQLObjects without inserting
### them on DB
##########################################################

class VirtualRow:
    """ This helper class fakes an
        SQLObject record without inserting it on db
    """

    def __init__(self, klass):
        self._dict = {}
        self._klass = klass
        for col in self._klass.sqlmeta.columns:
            self[col] = ''

    def insertAndGetRow(self):
        " insert record in db for real !!"
        if self._dict.has_key('id'):
            del self._dict['id']
        if hasattr(self._klass,'clearCache'):
            self._klass.clearCache()
        return self._klass(**self._dict)

    def __getitem__(self, key):
        " dict feature: access a given key "
        return self._dict[key]

    def __setitem__(self,key,value):
        " dict feature: set value for a given key "
        self._dict.update({key:value})

    def __delitem__(self,key):
        " dict feature: del item sotred at given key "
        del self._dict[key]

    def __call__(self):
        " dummy helper to get row storage "
        return self._dict

    def __getattr__(self, attr):
        " fake dict item as attribute "
        if attr != '_dict' and attr in self._dict.keys():
            return self._dict[attr]
        else:
            try:
                return self.__dict__[attr]
            except KeyError:
                raise AttributeError, attr

    def __setattr__(self, attr, value):
        " storing 'attributes' in dict "
        if attr not in ['_dict','_klass','_conn']:
            self._dict.update({attr:value})
        else:
            self.__dict__[attr] = value
