# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from sqlobject import connectionForURI
import sys
import time

#DEBUG=1
DEBUG=0

LOG_FILE='/tmp/log.sql'

__all__ = ['DBConnection','DEBUG','LOG_FILE']



class FileLogger:
    
    def __init__(self, fname):
        self.file = open(fname,'a+')    

    def __del__(self):
        self.close()

    def write(self, data):
        self.file.write(data)

    def close(self):
        self.file.close()

class DBConnection(object):

    def __init__(self, uri):
        """
        - `uri` : the database uri string (cf SQLObject)
        """
        self._uri = uri
        self._conn = connectionForURI(self._uri)

        self._debug = DEBUG
        self._logFile = LOG_FILE
        
        if self._conn.debug:
            # turn off default debug
            self._conn.debug = 0
            
        if self._debug:
            self._executeRetry = self._conn._executeRetry
            self._conn._executeRetry = self.__executeRetry
            
    def __getattr__(self, attr):
        " hook the wrapped SQLObject dbconnection " 
        conn = self._conn
        return getattr(conn, attr)

    def __executeRetry(self, conn, cursor, query):
        """ a custom query execution method

            if debug is activated, the query exec time and the query
            itself are logged.
        """
        logger = FileLogger(self._logFile)
        t0 = time.time()
        logger.write("query : %s\n" % query)
        result = self._executeRetry(conn,cursor, query)
        t1 = time.time()
        logger.write("[%.3fs] : %s\n" % (float(t1-t0),query))
        logger.close()            
        return result
