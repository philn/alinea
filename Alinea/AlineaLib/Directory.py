# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from quixote.directory import Directory as QuixoteDirectory

VERBOSE=0

def cls(instance):
    return instance.__class__.__name__

def log(msg):
    if VERBOSE:
        print msg

class Directory(QuixoteDirectory):

    def __init__(self, root=None):
        if root:
            self.setRoot(root)

    def rpc_process(self,meth, params):
        try:
            func = getattr(self, meth)
        except AttributeError:
            raise RuntimeError, "Unknown XML-RPC method: %r" % meth
        else:
            return apply(func,params)

    def getPage(self, name, klass=None):
        if not self.isRoot():
            # root acquisition
            root = self.getRoot()
        else:
            root = self
        
        log('Looking for "%s" in %s' % (name, cls(root)))
        if not hasattr(root, 'pages'):
            log('   Pages refresh in %s' % cls(root))
            root.pages = {}

        page = root.pages.get(name)
        if page:
            log('   Got page %s' % name)
        else:
            if klass:
                page = klass()
                log('   Storing page %s in %s' % (name, cls(root)))
                root.pages[name] = page
            else:
                log("   Page '%s' not found in %s.%s " % (name, cls(root),
                                                          root.pages.keys()))
                page = None

        if page:
            page.setRoot(root)
            
        return page

    def isRoot(self):
        return not hasattr(self, '_root')

    def setRoot(self, root):
        " set the root AlineaDirectory holding this object "
        self._root = root

    def getRoot(self):
        " get the root AlineaDirectory holding this object "
        root = self._root
        while hasattr(root,'_root'):
            root = root._root
        return root
