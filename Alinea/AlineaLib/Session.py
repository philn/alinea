# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from quixote.session import Session

__all__ = ['MySession', 'SessionPickle']

class MySession(Session):
    def __init__(self, id):
        Session.__init__(self, id)
        self.user = {}
    
    def __setitem__(self, it, val):
        self.user.update({it: val})

    def __getitem__(self, it):
        return self.user[it]

    def __delitem__(self, it):
        del self.user[it]

    def get(self, key, default=None):
        return self.user.get(key, default)

    def has_info(self):
        """ why the fucking damn hell default Quixote sessions should
        not have info ? Very instructive to spend hours debugging
        quixote sessions nightmare BTW.
        """
        return True

    value = get

################################################################################
# Session Management
# This is a bit crappy sessions .. look at the Quixote doc for another way
# http://www.mems-exchange.org/software/quixote/doc/session-mgmt.html
################################################################################
#SESSION_FILE='/tmp/qx_sessions'

import pickle

class SessionPickle:

    def __init__(self, sessionFile):
        self.sessionFile = sessionFile

    def loadSessions(self):
        print "Loading session file.."
        try:
            f = open(self.sessionFile,'rw')
            sessions = pickle.load(f)
            f.close()
        except IOError:
            return {} # first load ?
        return sessions


    def saveSessions(self, sessions):
        print "Saving sessions ..."
        f = open(self.sessionFile,'w')
        pickle.dump(sessions,f)
        f.close()

