"""
Reproduce the package structure of directory. Don't copy files, but
create empty __init__.py files so that python can import the packages.

Example of a created structure :

Alinea/phil_templates
|-- Articles
|   |-- __init__.py
|   |-- admin
|   |   `-- __init__.py
|   `-- archives
|       `-- __init__.py
|-- Comments
|   |-- __init__.py
|   `-- admin
|       `-- __init__.py
|-- Search
|   `-- __init__.py
|-- Sections
|   |-- __init__.py
|   `-- admin
|       `-- __init__.py
`-- Users
    |-- __init__.py
    `-- admin
        `-- __init__.py
"""

import os, sys
import shutil

def addInitFile(dirName):
    dirName = os.path.normpath(dirName)
    name = os.path.join(dirName,'__init__.py')
    if not os.path.exists(name):
        f = open(name,'w')
        f.close()

def mirror(src, dst):
    names = os.listdir(src)
    if not os.path.exists(dst):
        os.mkdir(dst)
    errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if os.path.isdir(srcname):
                mirror(srcname, dstname)
        except (IOError, os.error), why:
            errors.append((srcname, dstname, why))
    if not errors:
        addInitFile(dst)
    else:
        print errors
        
def mirrorDirs(inputDir, outputDir):
    print 'Mirror "%s" to "%s"' % (inputDir, outputDir)
    if os.path.exists(outputDir):
        shutil.rmtree(outputDir)
    
    os.makedirs(outputDir)
    mirror(inputDir, outputDir)

if __name__ == '__main__':
    mirrorDirs(sys.argv[1], sys.argv[2])
