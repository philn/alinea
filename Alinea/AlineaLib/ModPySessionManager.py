# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import copy
import gdbm
import cPickle
import time

# gdbmSession code comes from :
# http://www.scanmine.com/mp/mod_python_b_p.html

# Quixote
from quixote.session import SessionManager
from quixote import get_publisher, get_request

# mod_python
from mod_python import Cookie
from mod_python import Session
from mod_python import apache
_apache = apache._apache

# this is the mapping storing ModPyClasses by Quixote Session type
mod_py_sess_factory = {}

class gdbmSession(Session.DbmSession):
    """gdbm Session object
    The gdbm database is opened in 'cu' mode"""

    def __init__(self,req,SID=None,timeout=60):
        Session.DbmSession.__init__(self, req, dbm=None, sid=SID,
                                    secret=None, dbmtype=gdbm,
                                    timeout=timeout, lock=1)
        if not SID:
            self.createSession(req)
        else:
            self.getSameSession()

    def createSession(self,req):
        if self.is_new():
            self.save()
            self.unlock()
        return self.getSameSession()

    def getSameSession(self):
        self.do_load()
        return self

    def do_cleanup(self):
        data = [self._dbmfile, self._req.server]
        self._req.register_cleanup(dbm_cleanup, data)
        self._req.log_error("DbmSession: registered database cleanup.",apache.APLOG_NOTICE)

    def _get_dbm(self):
        result = gdbm.open(self._dbmfile, 'cu')
        return result


    def do_load(self):
        _apache._global_lock(self._req.server, None, 0)
        dbm = self._get_dbm()
        try:
            if dbm.has_key(self._sid):
                return cPickle.loads(dbm[self._sid])
            else:
                return None
        finally:
            dbm.close()
            _apache._global_unlock(self._req.server, None, 0)

    def do_save(self, dict):
        _apache._global_lock(self._req.server, None, 0)        
        dbm = self._get_dbm()
        try:
            dbm[self._sid] = cPickle.dumps(dict)
        finally:
            dbm.sync()
            dbm.close()
            _apache._global_unlock(self._req.server, None, 0)

def dbm_cleanup(data):
    dbm, server = data
    _apache._global_lock(server, None, 0)
    db = gdbm.open(dbm, 'cu')
    try:
        old = []
        key = db.firstkey()
        while key != None:
            val = db[key]
            dict = cPickle.loads(val)
            try:
                if (time.time() - dict["_accessed"]) > dict["_timeout"]:
                    old.append(key)
            except KeyError:
                old.append(key)

            try:
                key = db.nextkey(key)
            except KeyError: break

        for key in old:
            try:
                del db[key]
            except: pass
    finally:
        db.sync()
        db.reorganize()
        db.close()
        _apache._global_unlock(server, None, 0)

_ModPySession = gdbmSession

class ModPySession:
    """ Glue between Mod_Python Sessions and Quixote Sessions

    """

    # the Quixote Session class
    sessClass = None

    def __init__(self, id):
        """ Create the Quixote Session instance """
        self.sessInstance = self.sessClass(id)

    def __getattr__(self, attrName):
        """ Hook Quixote Session atttributes """
        return getattr(self.sessInstance, attrName)

    def setSessionClass(cls, sessClass):
        """ Map a Quixote Session class to this Mod_Python Session """
        cls.sessClass = sessClass

    setSessionClass = classmethod( setSessionClass )

    def setModPySession(self, modPySession):
        """ Map a Mod_Python Session to this Session """
        self.modPySession = modPySession

    def __setitem__(self, it, val):
        """ Store some data both in the Quixote Session and in the
            Mod_Python Session"""
        if hasattr(self.sessInstance, '__setitem__'):
            self.sessInstance[it] = val
        self.modPySession[it] = val

    def __getitem__(self, it):
        """ Hook Mod_Python Session items """
        return self.modPySession[it]

    def __delitem__(self, it):
        """ Hook Mod_Python Session item deletion """
        del self.modPySession[it]

    def get(self, key, default=None):
        """ Re-Hook default Quixote.Session.get to Mod_Python data access """
        return self.modPySession.get(key, default)

    value = get

    def save(self):
        """ Save the Mod_Python Session """
        self.modPySession.save()

    def is_dirty(self):
        """ That Quixote Session is durty for sure """
        return True

class ModPySessionManager(SessionManager):

    def __init__(self, session_class=None, session_mapping=None):

        if not mod_py_sess_factory.has_key(session_class):
            ModPySession.setSessionClass(session_class)
            mod_py_sess_factory[session_class] = copy.copy(ModPySession)

        klass = mod_py_sess_factory.get(session_class)
        SessionManager.__init__(self, session_class=klass,
                                session_mapping=session_mapping)

    def apache_log(self, msg):
        assert hasattr(self, 'modpython_request')
        self.modpython_request.log_error("SessionManager: %s" % msg,
                                         apache.APLOG_NOTICE)
        
    def __setitem__(self, session_id, session):
        """(session_id : string, session : Session)

        Store 'session' in the session manager under 'session_id'.
        """
        SessionManager.__setitem__(self, session_id, session)
        session.save()

    def __getitem__(self, session_id):
        return self.get(session_id)

    def maintain_session(self, session):
        """(session : Session)

        Maintain session information.  This method is called after servicing
        an HTTP request, just before the response is returned.  If a session
        contains information it is saved and a cookie dropped on the client.
        If not, the session is discarded and the client will be instructed
        to delete the session cookie (if any).
        """
        if not session.has_info():
            # Session has no useful info -- forget it.  If it previously
            # had useful information and no longer does, we have to
            # explicitly forget it.
            if session.id and self.has_session(session.id):
                del self[session.id]
                self.revoke_session_cookie()
            return

        if session.id is None:
            # This is the first time this session has had useful
            # info -- store it and set the session cookie.
            #session.id = self._make_session_id()
            session.id = session.modPySession._sid
            self[session.id] = session
            self.set_session_cookie(session.id)

        elif session.is_dirty():
            # We have already stored this session, but it's dirty
            # and needs to be stored again.  This will never happen
            # with the default Session class, but it's there for
            # applications using a persistence mechanism that requires
            # repeatedly storing the same object in the same mapping.
            self[session.id] = session
    
    def get(self, session_id, default=None):
        t0 = time.time()
        req = self.modpython_request
        result = default
        session = gdbmSession(req,session_id)
        
        try:
            data = session['_data']
        except KeyError:
            data = {}
        result = self.new_session(session_id)
        result.setModPySession(session)
        if data:
            result.sessInstance.user.update(data)
            if session_id:
                result.id = session_id
        t1 = time.time()
        return result

    def start_request(self, modpython_request):
        self.t0 = time.time()
        self.modpython_request = modpython_request
        SessionManager.start_request(self)

    def commit_changes(self, session):
        t1 = time.time()
        self.apache_log("Time: %s" % float(t1-self.t0))        
        self.modpython_request = None

    def _set_cookie(self, value, **attrs):
        """(session_id : string)

        Ensure that a session cookie with value 'session_id' will be
        returned to the client via the response object.

        Since Mod_Python has its own Cookie management system, we use it.
        """
        config = get_publisher().config
        name = config.session_cookie_name
        domain = config.session_cookie_domain

        if config.session_cookie_path:
            path = config.session_cookie_path
        else:
            path = get_request().get_environ('SCRIPT_NAME')
            if not path.endswith("/"):
                path += "/"

        expires = -1

        options = {'expires': expires,
                   'path': path }

        if domain is not None:
            options.update({'domain':domain})

        if value:
            Cookie.add_cookie(self.modpython_request, name, value, **options)

        return name

    def _get_session_id(self, config):
        """() -> string

        Find the ID of the current session by looking for the session
        cookie in the request.  Return None if no such cookie or the
        cookie has been expired, otherwise return the cookie's value.

        Since Mod_Python has its own Cookie management system, we use it.
        """
        cookies = Cookie.get_cookies(self.modpython_request)

        try:
            sessID = cookies[config.session_cookie_name].value
            if sessID in ("","*del*"):
                sessID = None
        except KeyError:
            # let's try with pysid
            sessID = self._get_session_id_from_pysid(cookies)
            
        return sessID

    def _get_session_id_from_pysid(self, cookies):

        try:
            sessID = cookies['pysid'].value
        except KeyError:
            sessID = None
        return sessID
