# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import os
import sys
import config

hl = "=" * 78 + '\n'
if not config.db_uri:
    print hl + "WARNING YOU NEED TO SET YOUR DB CONNECTION FIRST"
    print "please edit config.py"
    print "Exiting...\n" + hl
    import sys
    sys.exit()

sys.path.insert(0, config.compiled_templates_dir)
sys.path.insert(1, config.BASEDIR)

from packages import packages
from Alinea.AlineaLib.Directory import Directory
from Alinea.Search import SearchDirectory
#from Alinea.BlogRoll import BlogRollPage
from Alinea.AlineaLib.Hub import setConnection
from Alinea.AlineaLib.DBConnection import DBConnection
from Alinea.AlineaLib.FSCheetah import FSCheetah
from Alinea import __version__, __version_tuple__

from quixote.util import xmlrpc
from quixote.util import StaticDirectory
from quixote import get_request


import Welcome

PACKAGE_DIRS = {}

def _getPackages(root=None):
    if PACKAGE_DIRS == {}:
        for package in packages:
            dirName = "%sDirectory" % package
            mod = __import__("Alinea.%s.Directory" % package,
                             globals(),locals(),[''])
            
            try:
                directory = getattr(mod, dirName)
            except AttributeError,err:
                # no AlineaDirectory in this package ...
                if package != 'AlineaLib':
                    print 'Problem with the package "%s". Error is "%s". I continue my work' % (package, err)
                continue
            theDirectory = directory(root)
            PACKAGE_DIRS[package] = theDirectory
    return PACKAGE_DIRS

class RPCDirectory(Directory):

    def _q_lookup(self, component):
        packagesDict = _getPackages()
        directory = packagesDict[component]
        result = xmlrpc(get_request(), directory.rpc_process)
        return result

    def _q_traverse(self, path):
        assert len(path) > 0
        component = path[0]
        path = path[1:]
        name = self._q_translate(component)
        if name is not None:
            obj = getattr(self, name)
        else:
            obj = self._q_lookup(component)
        if obj is None:
            raise TraversalError('directory %r has no component %r' %
                                 (self, component))
        return obj

class TemplatesDirectory(Directory):

    def _q_lookup(self, component):
        name = component
        try:
            tmpl = FSCheetah(name)
            data = tmpl.src()
        except:
            data = "This doesn't look like a template ... or it's not public"
            
        request = get_request()
        request.response.set_content_type('text/plain')
        
        return data

class LanguagesDirectory(Directory):

    def _q_lookup(self, lang):
        request = get_request()
        request.session['language'] = lang
        referer = request.get_environ('HTTP_REFERER')
        if not referer:
            referer = config.site_url
        return request.response.redirect(referer)

class AlineaDirectory(Directory):

    _q_exports = ['','_q_index', 'Error', 'admin', 'login', 'logout', 'rpc',
                  'Search','Languages', 'Templates','static', 'actions'] + packages
    
    rpc = RPCDirectory()

    static = StaticDirectory(config.static_content_dir, list_directory=1,
                             use_cache=1, cache_time=600)
    Search = SearchDirectory()
    Languages = LanguagesDirectory()
    Templates = TemplatesDirectory()
    
    def __init__(self):
        Directory.__init__(self)
        for package, directory in _getPackages(self).iteritems():
            setattr(self, package, directory)
        self.setupDBConnection()

    def _q_index(self):
        """ redirect to the Welcome Page """
        page = self.getPage('Welcome', Welcome.WelcomePage)
        request = get_request()
        return page.handle(request)

    def actions(self):
        page = self.getPage('Welcome', Welcome.WelcomePage)
        request = get_request()
        return page.actions()

    def login(self):
        import Login
        page = Login.LoginPage()
        request = get_request()
        return page.handle(request)

    def logout(self):
        import AdminWelcome
        page = AdminWelcome.WelcomePage()
        request = get_request()
        return page.logout(request)

    def admin(self):
        import AdminWelcome
        page = AdminWelcome.WelcomePage()
        request = get_request()
        return page.handle(request)

    def setupDBConnection(self):
        setConnection(DBConnection(config.db_uri))
