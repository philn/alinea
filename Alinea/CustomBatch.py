# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from AdminPage import AdminPage

class CustomBatch(AdminPage):

    """ This tends to define a common behaviour for all
        Servlets wanting to display huge informations like
        Articles, Comments, Sections.

        Look at ``templates.AdminPage.tmpl`` for few usefull 'widgets':

        - prevButton
        - pageNb
        - nextButton
        - editBox (for display options)

        Options are stored on session following this schema:

        { servLetName: { 'orderBy': 'date',
                         'reversed': True,
                         ...
                       }
          ...
        }

        Available options are:

        - `orderBy` : sorting attribute
        - `reversed`: is the selection reversely sorted or not ?
        - `range`: number of item to display per page

        Following informations are also stored:

        - `size`: length of all items to display
        - `begin`: list index of the first item of the page

        User-defined (inheritated) classes have to redefine the SERVLET
        variable with their name.
    """

    REVERSE=False
    DEFAULT_RANGE=10
    DEFAULT_ORDERBY='id'
    SERVLET = 'CustomBatch'
    _template = ''
    _actions = [ 'prev','next', 'Reverse', 'UnReverse' ]

    def __init__(self):
        AdminPage.__init__(self, self._template)

    #######################################################
    ### Servlet behaviour
    #######################################################

    def render(self):
        range = self.requestValue('range')
        if range:
            self.setRange(int(range))
        orderBy = self.requestValue('orderBy')
        if orderBy:
            self.setOrderBy(orderBy)
        size = self.getSize()
        if self.getBegin() >= size and self.getPageTotal() > 1:
            # do no go too far
            self.setBegin(size-self.getRange())
        return AdminPage.render(self)

    def store(self, key, item):
        """ store information on session() item mapped to
            the servlet name """
        opts = self.getFromSession(self.SERVLET,{})
        if opts == {}:
            self.storeToSession(self.SERVLET,{})
            opts = self.getFromSession(self.SERVLET)
        opts[key] = item
        self.storeToSession(self.SERVLET,opts)

    def get(self, key, default=None):
        """ Retrieving information from session() item
            mapped to servlet name """
        opts = self.getFromSession(self.SERVLET,{})
        if opts == {}:
            self.store(key,default)
        return opts.get(key,default)

    #######################################################
    ### Session informations
    #######################################################

    def setReverse(self, value):
        self.store('reverse',value)

    def getReverse(self):
        return self.get('reverse',self.REVERSE)

    def setOrderBy(self, orderBy):
        self.store('orderBy',orderBy)

    def getOrderBy(self):
        " current articles ordering directive "
        return self.get('orderBy',self.DEFAULT_ORDERBY)

    def setRange(self, value):
        self.store('range',value)

    def getRange(self):
        " current range "
        result = self.get('range',self.DEFAULT_RANGE)
        return int(result)

    def setBegin(self, begin=0):
        self.store('begin',begin)

    def getBegin(self):
        " current index in articles list "
        result = self.get('begin','0')
        return int(result)

    def setSize(self, size=0):
        self.store('size',size)

    def getSize(self):
        " articles list size"
        result = self.get('size')
        return int(result)

    #######################################################
    ### Actions
    #######################################################

    def Reverse(self, request):
        self.setReverse(True)
        return self.redirect(request)

    def UnReverse(self, request):
        self.setReverse(False)
        return self.redirect(request)

    def next(self, request):
        " next page "
        self.setBegin(self.getBegin() + self.getRange())
        return self.redirect(request)

    def prev(self, request):
        " previous page "
        self.setBegin(self.getBegin() - self.getRange())
        return self.redirect(request)

    #######################################################
    ### Servlet Specific options
    #######################################################

    def ranges(self):
        return [ self.DEFAULT_RANGE ]

    def orders(self):
        return [ self.DEFAULT_ORDERBY ]

    #######################################################
    ### Misc
    #######################################################

    def getPageNb(self):
        " current page number "
        return (self.getBegin() / self.getRange()) + 1

    def getPageTotal(self):
        " number of pages "
        result, mod = divmod(self.getSize(), self.getRange())
        if mod > 0:
            result += 1
        return result
