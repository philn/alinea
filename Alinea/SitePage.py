# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import sys,os, warnings, urllib2, time
import urllib
import config
from Alinea.AlineaLib import FSCheetah, DBConnection
from Alinea.AlineaLib.Directory import Directory
from Alinea.AlineaLib.Languages import Languages
from quixote import get_request, get_session, get_publisher
import time, calendar, datetime
from Alinea import __version__

from Articles.AlineaArticle import AlineaArticle,Alinea_getArticlesForMonth

import socket
socket.setdefaulttimeout(5)

##############################################################################
# SitePage
##############################################################################

LANGUAGES = Languages()

class SitePage(Directory,FSCheetah.FSCheetah):
    """ General Mixing to be the view """

    _contentType = None
    _actions = ['nextMonth','prevMonth','processQuery']
    _q_exports = [ '','actions' ]

    def __init__(self,template):
        self._master = FSCheetah.FSCheetah('templates.SitePage')
        FSCheetah.FSCheetah.__init__(self, 'templates.%s' % template)
        Directory.__init__(self)
        
    def objectID(self):
        return None

    def version(self):
        return __version__
    
    def config(self):
        return config
    
    def getBlogTitle(self):
        return self.config().site_name

    def getBlogUrl(self):
        return self.config().site_url

    def getBlogDesc(self):
        return self.config().site_description

    def getBlogLang(self):
        return self.config().site_lang

    def getBlogCopyright(self):
        return self.config().site_copyright

    def getLinks(self):
        return self.config().links

    def getBaseURL(self):
        return self.config().site_url

    def getStaticURL(self):
        return self.config().static_url

    def getSessionID(self):
        return get_session().id

    def getSectionsAsTree(self):
        section = self.getRootSection()
        if section:
            return section.asTree()
        else:
            return []

    def getSections(self):
        section = self.getRootSection()
        if section:
            return section.getChildren()
        else:
            return []
        

    def getRootSection(self):
        from Sections.AlineaSection import Alinea_getRootSection
        rootSection = Alinea_getRootSection()
        return rootSection

    def getPopularArticles(self):
        # ,orderBy='-hits'
        return AlineaArticle.select('all', orderBy=-AlineaArticle.q.hits)[:10]

    def getArticlesForMonth(self,  year, month):
        return Alinea_getArticlesForMonth(year, month)        

    def handle(self,request):
	redir = self.awakePage(request)
	#if redir:
	#    return redir
        data = self.render()
        self.sleep(request)
        data = data.strip()
        return data

    def _q_index(self):
        request = get_request()
        return self.handle(request)

    ###########################################################################
    ### Cheetah 
    ###########################################################################

    def refreshCache(self, cacheRegionId, cacheId=None):
        self._master.refreshCache(cacheRegionId, cacheId)
        FSCheetah.FSCheetah.refreshCache(self, cacheRegionId,cacheId)

    def refreshPageCache(self, pageName, cacheRegionId, cacheId=None):
        page = self.getPage(pageName)
        if page:
            page.refreshCache(cacheRegionId,cacheId)
            
    def render(self):
        #hack to permit the reload of the master template

        t0 = time.time()
        
        # try to set the custom template
        oldTmpl = None
        objID = self.objectID()
        if objID is not None:
            oldTmpl = self.get_template_name()
            if self.set_template_name(objID):
                print 'Loaded custom template : %s' % objID
                
        if FSCheetah.FSCHEETAH_AUTO_RELOAD:
            if self._master.changed():
                if FSCheetah.FSCHEETAH_AUTO_COMPILE:
                    self._master.build()
                reload(self._master.module())
                reload(self._module)
                
        # render the template
        result = FSCheetah.FSCheetah.render(self)

        # restore the main page template
        if oldTmpl:
            self.set_template_name(oldTmpl)

        #print "Rendering time : %s" % str(time.time() - t0)
        return result

    ###########################################################################
    ### Quixote request
    ###########################################################################

    def awakePage(self,request):
        """ we are setting the request """
        self.request = request
        default = self.getDefaultLanguage()
        lang = self.getFromSession('language', default)
        self._ = LANGUAGES.gettextFunc(lang)

        if self._contentType:
            self.request.response.set_content_type(self._contentType)
            
    def sleep(self,request):
        self.request = None
        
    def redirectAfterForm(self, request, formName='actions'):
        url = request.get_environ('SCRIPT_URI')
        if not url:
            url = request.get_url()
        method, server, path, d1, d2, d3 = urllib2.urlparse.urlparse(url)

        path = path.split(formName)[0]
        url = urllib2.urlparse.urlunparse((method, server, path, d1,d2,d3))
        return request.response.redirect(url)

    def redirect(self, request, location=None):
        if location is None:
            location = self._location
        url = '%s%s' % (self.getBaseURL(), location)
        print 'Redirecting ... ', url
        return request.response.redirect(url)

    def requestValue(self,name,default=None):
        try:
            value = self.request.form[name]
        except KeyError:
            value = default
        return value

    
    ### Message ###
    def setMessage(self,message):
        self.storeToSession('message', message)

    def delMessage(self):
        self.delFromSession('message')

    def getMessage(self):
        msg = self.getFromSession('message')
        self.delMessage()
        return msg

    def actions(self):
        " available servlet actions"
        request = get_request()
        self.awakePage(request)
        formActions = request.form.keys()
        for action in self._actions:
            if action in formActions and self.checkRole(action):
                return getattr(self, action)(request)
        print 'Action "%s" not found .. ' % str(formActions)
        return self.handle(request)

    def checkRole(self, action):

        return True

    ###########################################################################
    ### Session Management
    ###########################################################################

    def saveSession(self):
        " forcing storing of Session "
        warnings.warn("Deprecated ... don't use that anymore")

    def getFromSession(self, key, default = None):
        " fetching information mapped to key from Session "
        if self.request.session is not None:
            val = self.request.session.value(key, default)
            return val
        else:
            return default

    def delFromSession(self, key):
        " delete item mapped to key from Session "
        item = self.getFromSession(key)
        if item:
            del self.request.session[key]

    def storeToSession(self, key, value):
        " put a value mapped to a key in Session "
        if self.request.session:
            self.request.session[key] = value

    ###########################################################################
    ### Calendar Management
    ###########################################################################

    def setYear(self, year):
        self.storeToSession('year', year)

    def setMonth(self, month):
        self.storeToSession('month', month)

    def getYear(self):
        thisYear = datetime.datetime.today().year
        return self.getFromSession('year', thisYear)

    def getMonth(self):
        thisMonth = datetime.datetime.today().month
        month = self.getFromSession('month',thisMonth)
        return month

    def nextMonth(self, request):
        month = self.getMonth()
        if month < 12:
            month += 1
        else:
            month = 1
            self.setYear(self.getYear() + 1)
        self.setMonth(month)
        return self.redirectAfterForm(request)

    def prevMonth(self, request):
        month = self.getMonth()
        if month > 1:
            month -= 1
        else:
            self.setYear(self.getYear() -1)
            month = 12
        self.setMonth(month)
        return self.redirectAfterForm(request)

    def nextYear(self, request):
        year = self.getYear()
        self.setYear(year + 1)
        return self.redirectAfterForm(request)

    def prevYear(self, request):
        year = self.getYear()
        self.setYear(year - 1)
        return self.redirectAfterForm(request)


    def getMonthName(self, monthNb):
        """ Get localized month name.

            Build a 'fake' datetime object and format it.
        """
        return datetime.datetime(2004,monthNb,01).strftime('%B')[:3]

    def getWeekDaysNames(self):
        """ Get localized week day names.

            Return a list of strings giving days names depending on
            default locale.
        """
        today = datetime.datetime.today()
        firstDay = today - datetime.timedelta(days=today.weekday())
        days = [ (firstDay + datetime.timedelta(days=idx)).strftime('%A')
                 for idx in range(7)]
        return days

    def getWeeks(self, year, month):
        """ Get all weeks of a month in a list.

            Each week is a list of days. Each day is a tuple:

            ::

              (dayNumber, linkToDayArchive, cssClass)
        """
        year, month, time.localtime()[:2]
        cal = calendar.monthcalendar(year, month)

        weeks = []
        articlesByDays = self.getArticlesForMonth(year, month)
        for week in cal:
            days = []
            for dayIndex in range(len(week)):
                if dayIndex in (0, 6):
                    klass = 'wkend'
                else:
                    klass = 'wkday'
                day = week[dayIndex]
                link = ''

                articlesNb = articlesByDays.get(day,0)
                if day == 0:
                    day = ''
                elif articlesNb > 0:
                    # hum we need to discuss about that URL mapping
                    link = '%s/Articles/archives/%s_%s_%s/' % (self.getBaseURL(),year, month, day)
                days.append((day,link,klass))
            weeks.append(days)
        return weeks


    ###########################################################################
    ### Misc
    ###########################################################################

    def processQuery(self, request):
        query = self.requestValue('query')
        return self.redirect(request,urllib.quote('/Search/%s' % query))

    def _getEntriesFromFileCache(self, filename, url, minuts=10):
        now = int(time.time())
        
        # update the cache if last fetch's age exceeds 10 minuts
        try:
            wantUpdate = now - os.stat(filename).st_mtime > (60 * minuts)
        except OSError:
            wantUpdate = True

        if wantUpdate:
            try:
                entries = urllib2.urlopen(url).read()
            except:
                entries = ''
            else:
                d = open(filename,'w')
                d.write(entries)
                d.close()
        else:
            d = open(filename)
            entries = d.read()
            d.close()
        return entries
        
    def deliciousEntries(self):
        deliLogin = self.config().deliciousLogin
        if deliLogin == '':
            return ''
        deliCache = os.path.join('/tmp','delicious.%s.tmp' % deliLogin)
        deliURL = 'http://del.icio.us/html/%s?tags=no&rssbutton=no' % deliLogin
        return self._getEntriesFromFileCache(deliCache, deliURL)
    
    def deliciousURL(self):
        deliLogin = self.config().deliciousLogin
        return "http://del.icio.us/%s" % deliLogin

    def gvisitURL(self):
        md5 = self.config().gvisitHash
        return "http://www.gvisit.com/map.php?sid=%s" % md5

    def lastVisitors(self):
        gHash = self.config().gvisitHash
        if gHash == '':
            return ''
        gURL = "http://www.gvisit.com/plain.php?sid=%s" % gHash
        gCache = os.path.join('/tmp','gvisit.%s.tmp' % gHash)
        lines = self._getEntriesFromFileCache(gCache, gURL, 50).splitlines(1)
	try:
	    line = lines[28]
	    visits = '\n'.join([ l.split(': ')[1] for l in line.split('<br>')[2:-1] ])
	except:
	    visits = ''
        return visits.splitlines(1)

    def lastPlayedSongs(self):
        asLogin = self.config().audioScrobblerLogin
        if asLogin == '':
            return ''
        asCache = os.path.join('/tmp', 'audioscrobbler.%s.tmp' % asLogin)
        asURL = 'http://ws.audioscrobbler.com/1.0/user/%s/recenttracks.txt' % asLogin	
        rawData = self._getEntriesFromFileCache(asCache, asURL)
	try:
	    result = ''.join([line.split(',')[1] for line in rawData.splitlines(1)])
	except:
	    result = ''
        return result.splitlines(1)
    
    def audioScrobbleProfileURL(self):
        asLogin = self.config().audioScrobblerLogin
        if asLogin == '':
            url = '#'
        else:
            url = 'http://www.last.fm/user/pnormand/'
        return url
    
    def getLanguages(self):
        default = self.getDefaultLanguage()
        return [ default, ] + LANGUAGES.availableLanguages()

    def getDefaultLanguage(self):
        # XXX: this should probably be tweakable via the ALINEA_CONFIG
        return 'en'

