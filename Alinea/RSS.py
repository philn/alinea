# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.SitePage import SitePage
from AlineaLib import FSCheetah
import time
import datetime

class RSS(SitePage):

    _contentType = 'application/xml'

    def __init__(self,template):
        SitePage.__init__(self, template)
        self._master = FSCheetah.FSCheetah('templates.RSSPage')

    getFeedTitle = SitePage.getBlogTitle
    getFeedDesc = SitePage.getBlogDesc
    getFeedUrl = SitePage.getBlogUrl
    getFeedLang = SitePage.getBlogLang
    getFeedCopyright = SitePage.getBlogCopyright

    def formatDate(self, aDate):

	# get delta between now and UTC time
	now = datetime.datetime.now()
	utcnow = datetime.datetime.utcnow()
	delta = now - utcnow

        # round the delta seconds to minutes
	minutes = int(round(delta.seconds / 60.))

        # buid a timedelta which will be used by the TZ instance
	delta = datetime.timedelta(minutes=minutes)

        # our custom TZ
	class TZ(datetime.tzinfo):
            def utcoffset(self, dt): return delta

	# build a new date, adding our TZ
	newDate = datetime.datetime(*aDate.timetuple()[:-2],**{'tzinfo':TZ()})
	return newDate.isoformat()
