# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.SitePage import SitePage
from AlineaLib import FSCheetah
import time

class Atom(SitePage):

    _contentType = 'application/xml'

    def __init__(self,template):
        SitePage.__init__(self, template)
        self._master = FSCheetah.FSCheetah('templates.AtomPage')

    getFeedTitle = SitePage.getBlogTitle
    getFeedDesc = SitePage.getBlogDesc
    getFeedUrl = SitePage.getBlogUrl
    getFeedLang = SitePage.getBlogLang
    

    def formatDate(self, aDate):
        aDate = time.mktime(aDate.timetuple())
        #return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(aDate))
        return time.strftime("%B %d, %Y %I:%M %p", time.gmtime(aDate))
