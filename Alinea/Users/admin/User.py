# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AdminPage import AdminPage
from Alinea.Users import AlineaUser
from Alinea.AlineaLib.AlineaTools import VirtualRow
import random
from quixote.errors import AccessError
from quixote import get_request


class UserDirectory(AdminPage):

    _actions = ['save','delete','cancel']
    _rolesRequired = {'delete':['admin'],
                      'save': ['admin']}
    
    def __init__(self):
        AdminPage.__init__(self,'Users.admin.User')

    def title(self):
        user = self.getUser()
        return "User edit : " + user.login

    def _q_lookup(self, component):
        if not component.startswith('new'):
            try:
                component = int(component)
            except:
                raise ValueError, 'No User ?'
        else:
            component = 'new%s' % random.randint(1,2000)
        self.userID = component
        return self

    def awakePage(self, request):
        user = self.getLoggedUser()
        if user:
            userRoles = [ r.label for r in user.roles ]
            if not self.isNew() and user.id != self.userID and \
                   'admin' not in userRoles:
                raise AccessError("You are not allowed to access to this user's settings")
	return AdminPage.awakePage(self, request)

    def handle(self, request):
        self.request = get_request()
        if self.isNew():
            user = self.newUser()
	redir = AdminPage.handle(self, request)
	if redir:
	    return redir

    def isNew(self):
        return type(self.userID) != type(0) and self.userID.startswith('new')

    def newUser(self):
        user = VirtualRow(AlineaUser.AlineaUser)
        user.id = self.userID
        user.articles = []
        user.roles = []
        users = self.getFromSession('editingUsers', {})
        users[user.id] = user
        self.storeToSession('editingUsers', users)
        return user
    
    def getUser(self, uid=None):
        if uid is None:
            uid = self.userID
        if type(uid) == type(0):
            user = AlineaUser.Alinea_getUserWithID(uid)
        else:
            users = self.getFromSession('editingUsers', {})
            print users
            user = users.get(uid)
        return user

    def delUserFromSession(self, user):
        """ editing users home-cleanup.

            Drop an user from the editingUsers hash.
        """
        users = self.getFromSession('editingUsers',{})
        try:
            del users[user.id]
        finally:
            self.storeToSession('editingUsers', users)

    def getAllRoles(self):
        return AlineaUser.AlineaRole.select()

    def save(self, request):
        uid = self.requestValue('id')
        try:
            uid = int(uid)
        except:
            pass

        user = self.getUser(uid)

        firstName = self.requestValue('firstName')
        lastName = self.requestValue('lastName')
        login = self.requestValue('login')
        email = self.requestValue('email')
        password = self.requestValue('password')
        password2 = self.requestValue('password2')
        roles = self.requestValue('roles')

        if roles is None:
            roles = []
        elif type(roles) != type([]):
            roles = [ roles ]
        roles = [ int(r) for r in roles ]

        user.firstName = firstName
        user.lastName = lastName
        user.login = login
        user.email = email
        if password == password2:
            user.passwd = password
        else:
            self.setMessage("Passwords don't match. Try again")
            return request.response.redirect(user.admin_url())

        if isinstance(user,VirtualRow):
            del user['articles']
            del user['roles']
            self.delUserFromSession(user)
            del user['id']
            user = AlineaUser.Alinea_addUser(login, firstName, lastName, password,email)
            #user = user.insertAndGetRow()
            
        try:
            AlineaUser.Alinea_editRoles(user, self.getLoggedUser(), roles)
        except ValueError:
            raise AccessError("You can't edit your roles")            

        self.setMessage("User '%s' saved" % user.login)
        return request.response.redirect(user.admin_url())

    def delete(self, request):
        uid = self.requestValue('id')
        try:
            uid = int(uid)
        except:
            user = self.getUser(uid)
            self.delUserFromSession(user)
        else:
            user = self.getUser(uid)
            AlineaUser.Alinea_delUser(user)
        return self.cancel(request)

    def cancel(self, request):
        return request.response.redirect(self.getUsersURL())
