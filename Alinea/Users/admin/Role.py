# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AdminPage import AdminPage
from Alinea.Users.AlineaUser import *
from Alinea.AlineaLib.AlineaTools import VirtualRow
import random

class RoleDirectory(AdminPage):

    _actions = ['save','delete','cancel']
    _rolesRequired = {'save':['admin'], 'delete':['admin']}

    def __init__(self):
        AdminPage.__init__(self, 'Users.admin.Role')

    def _q_lookup(self, component):
        if not component.startswith('new'):
            try:
                component = int(component)
            except:
                raise ValueError('No Role ?')
        else:
            component = 'new%s' % random.randint(1,2000)
        self.roleID = component
        return self


    def handle(self, request):
        self.awakePage(request)
        if self.isNew():
            role = self.newRole()
        return AdminPage.handle(self, request)

    def isNew(self):
        return type(self.roleID) != type(0) and self.roleID.startswith('new')

    def newRole(self):
        role = VirtualRow(AlineaRole)
        role.id = self.roleID
        role.users = []
        role.label = ''
        roles = self.getFromSession('editingRoles', {})
        roles[role.id] = role
        self.storeToSession('editingRoles', roles)

    def getRole(self, uid=None):
        if uid is None:
            uid = self.roleID
        if type(uid) == type(0):
            role = Alinea_getRoleWithID(uid)
        else:
            roles = self.getFromSession('editingRoles', {})
            role = roles.get(uid)
        return role

    def getUsers(self):
        return AlineaUser.select()

    def delRoleFromSession(self, role):
        roles = self.getFromSession('editingRoles',{})
        try:
            del roles[role.id]
        finally:
            self.storeToSession('editingRoles', roles)

    def save(self, request):
        uid = self.requestValue('id')
        try:
            uid = int(uid)
        except:
            pass

        role = self.getRole(uid)

        label = self.requestValue('label')
        users = self.requestValue('users')

        if users is None:
            users = []
        elif type(users) != type([]):
            users = [ users ]
        users = [ int(r) for r in users ]

        role.label = label

        if isinstance(role,VirtualRow):
            del role['users']
            self.delRoleFromSession(role)
            del role['id']
            role = role.insertAndGetRow()

        currentUsers = []
        for userId in users:
            user = Alinea_getUserWithID(userId)
            if user not in role.users:
                role.addAlineaUser(user)
            currentUsers.append(user)

        for user in role.users:
            if user not in currentUsers:
                role.removeAlineaUser(user)

        return request.response.redirect(role.admin_url())

    def delete(self, request):
        uid = self.requestValue('id')
        try:
            uid = int(uid)
        except:
            role = self.getRole(uid)
            self.delRoleFromSession(role)
        else:
            role = self.getRole(uid)
            Alinea_delRole(role)
        return self.cancel(request)

    def cancel(self, request):
        return request.response.redirect(self.getRolesURL())
