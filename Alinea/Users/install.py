# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.Users.AlineaUser import *

def setup(connection=None):
    if connection:
        AlineaUser._connection = connection
        AlineaRole._connection = connection
        
    AlineaUser.dropTable(ifExists=True,cascade=True)
    AlineaUser.createTable()

    AlineaRole.dropTable(ifExists=True,cascade=True)
    AlineaRole.createTable()

    # create the admin role
    adminRole = AlineaRole(label='admin')

    # create the publisher role
    pubRole = AlineaRole(label='publisher')

    # create the author role
    authRole = AlineaRole(label='author')

    # create the 'main' admin user
    login = 'admin'
    passwd = '0adm1n4tor'
    adminUser = AlineaUser(firstName='Alinea',lastName='Administrator',
                           email='alinea@localhost',login=login,passwd=passwd,
                           session='')
    adminUser.addAlineaRole(adminRole)
    adminUser.addAlineaRole(pubRole)
    adminUser.addAlineaRole(authRole)
    print """

Created admin user (%s/%s).
Use this account to manage users and respective roles

""" % (login,passwd)
