# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from sqlobject.sqlbuilder import AND
from sqlobject import *
from Alinea.AlineaLib.AlineaTools import getObjectWithClassAndID
from Alinea.AlineaLib.AlineaObject import AlineaObject
from Alinea.config import site_url

__all__ = ['AlineaUser', 'Alinea_getUserWithID', 'Alinea_delUser',
           'AlineaRole', 'Alinea_getRoleWithID', 'Alinea_delRole' ]

##########################################################
### Alinea User
##########################################################

class AlineaUser(AlineaObject):
    firstName = StringCol(length=128)
    lastName  = StringCol(length=128)
    email     = StringCol(length=128,default='')
    login     = StringCol(length=128,alternateID=True)
    passwd    = StringCol(length=128)
    session   = StringCol(length=64,default='')
    articles  = MultipleJoin('AlineaArticle')
    roles     = RelatedJoin('AlineaRole')

    def public_url(self):
        """ return the full url for an user """
        return site_url + '/Users/%s/' % self.id

    def admin_url(self):
        """ return the user URL in admin site view """
        return site_url + '/Users/admin/User/%s/' % self.id

AlineaUser.sqlmeta.cacheValues = True

def Alinea_getUserWithID(id):
    """ return the User w/ a given ID or None """
    return getObjectWithClassAndID(AlineaUser,id)

def Alinea_addUser(login, firstName, lastName, password,email):
    AlineaUser.clearCache()
    return AlineaUser(login=login, firstName=firstName, lastName=lastName,
                      passwd=password,email=email)

def Alinea_delUser(user):
    " cascade delete user articles "
    for article in user.articles:
        Alinea_delArticle(article)
    AlineaUser.delete(id=user.id)
    AlineaUser.clearCache()

def Alinea_doLogin(login, password):
    users = AlineaUser.select( AND( AlineaUser.q.login == login,
                                    AlineaUser.q.passwd == password ))
    if users.count() > 0:
        user = users[0]
    else:
        user = None
    return user

##########################################################
### Alinea Role
##########################################################

class AlineaRole(AlineaObject):
    label = StringCol(length=128)
    users = RelatedJoin('AlineaUser')

    def admin_url(self):
        """ return the user URL in admin site view """
        return site_url + '/Users/admin/Role/%s/' % self.id

def Alinea_getRoleWithID(roleID):
    return getObjectWithClassAndID(AlineaRole, roleID)

def Alinea_delRole(role):
    for user in role.users:
        role.removeAlineaUser(user)
    AlineaRole.delete(id=role.id)
    AlineaRole.clearCache()
    
def Alinea_editRoles(user, editor, roles):
    
    # roles editing is restricted to admin
    hasAdminRole = 'admin' in [ r.label for r in user.roles ]
    if not hasAdminRole:
        hasAdminRole = 'admin' in [ r.label for r in editor.roles ]

    if roles != [role.id for role in user.roles]:
        if not hasAdminRole:
            raise ValueError("You can't edit roles.. Ask to the admin")

    currentRoles = []
    for roleId in roles:
        role = Alinea_getRoleWithID(roleId)
        if role not in user.roles:
            user.addAlineaRole(role)
        currentRoles.append(role)

    for role in user.roles:
        if role not in currentRoles:
            user.removeAlineaRole(role)
    
