# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AlineaLib.Directory import Directory
from admin import UsersAdminDirectory

class UsersDirectory(Directory):
    _q_exports = ['','admin']

    admin = UsersAdminDirectory()
    
    def _q_index(self):
        return "We don't sell our users database. Move along please."

    def _q_lookup(self, component):
        try:
            userID = int(component)
        except:
            raise ValueError, "No User ?"
        from User import UserPage
        page = UserPage(userID)
        return page
