# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.RSS2 import RSS2
from Alinea.Comments.AlineaComment import AlineaComment

class CommentsRSS2Page(RSS2):

    def __init__(self):
        RSS2.__init__(self,'Comments.RSS2')

    def getComments(self, orderBy='date'):
        " return the last 10 comments "
        comments = list(AlineaComment.select('all',orderBy=orderBy, reversed=True)[0:10] )
        return comments
    
    def getFeedTitle(self):
        title = RSS2.getFeedTitle(self)
        return "%s / Comments" % title
