# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AlineaLib.Directory import Directory
from quixote import get_request
from admin import CommentsAdminDirectory
from Comment import Comment, NewComment

class CommentsDirectory(Directory):
    _q_exports = ['Comment', 'admin',
                  ('rss.xml','indexRSS'), ('rss2.xml','indexRSS2'),
                  ('atom.xml','indexAtom') ]

    def __init__(self, root):
        Directory.__init__(self, root)
        self.admin = CommentsAdminDirectory(root)

    def _q_lookup(self, component):
        try:
            commentID = int(component)
            #page = Comment(commentID)
            #page.setRoot(self.getRoot())
            page = self.getPage('Comment', Comment)
            page.setCommentID(commentID)
        except:
            parts = component.split('_')
            if len(parts) != 3:
                return """\
This shall be the Comments index page.
Please supply a Comment ID as last part of the url"""
            page = self.getPage('NewComment', NewComment)
            page.setObjectType(parts[-2])
            page.setObjectID(parts[-1])
        return page

    def indexRSS(self):
        from RSS import CommentsRSSPage
        return CommentsRSSPage().handle(get_request())

    def indexRSS2(self):
        from RSS2 import CommentsRSS2Page
        return CommentsRSS2Page().handle(get_request())

    def indexAtom(self):
        from Atom import CommentsAtomPage
        return CommentsAtomPage().handle(get_request())
    
