# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AlineaLib.Directory import Directory
from quixote import get_request
from Comment import CommentDirectory

class CommentsAdminDirectory(Directory):
    _q_exports = ['','Comment','actions']

    #Comment = CommentDirectory()

    def __init__(self, root):
        Directory.__init__(self, root)
        self.Comment = CommentDirectory(root)
        
    def _q_index(self):
        from Comments import CommentsPage
        v = CommentsPage()
        return v.handle(get_request())

    def actions(self):
        from Comments import CommentsPage
        return CommentsPage().actions()
