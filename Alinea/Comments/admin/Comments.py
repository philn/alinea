# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.CustomBatch import CustomBatch
from Alinea.Comments.AlineaComment import AlineaComment
from Alinea.Users.AlineaUser import AlineaUser

class CommentsPage(CustomBatch):

    _template = 'Comments.admin.Comments'
    _location = '/Comments/admin/'

    DEFAULT_ORDERBY= 'date'
    DEFAULT_RANGE=20
    REVERSE=True
    SERVLET = 'Comments'
    _q_exports = [ 'actions' ]
    _actions = CustomBatch._actions + ['delete']

    def getSize(self):
        query = AlineaComment.select()
        size = query.count()
        return size

    def title(self):
        return 'Comments admin'

    def ranges(self):
        " display range list "
        return [ self.DEFAULT_RANGE, 30, 40, 50, 60 ]

    def orders(self):
        " orderBy list"
        return [ self.DEFAULT_ORDERBY, 'id','email','url',
                 'ip', 'author' ]

    def getComments(self):
        """ Return the list of articles """
        begin = self.getBegin()
        end = begin + self.getRange()
        orderBy = self.getOrderBy()
        reverse = self.getReverse()
        comments = list(AlineaComment.select('all', orderBy=AlineaComment.q.date,
                                             reversed=reverse)[begin:end] )
        return comments

    def getAllComments(self):
        return list(AlineaComment.select('all',orderBy='id'))

    def delete(self, request):
        comments = self.requestValue('comments')

        if comments is None:
            comments = []
        elif type(comments) != type([]):
            comments = [ comments ]

        deleted = [ AlineaComment.delete(int(commentID))
                    for commentID in comments ]
        self.setMessage("Deleted %d comments" % len(deleted))
        begin = self.getBegin()
        return self.redirect(request)
