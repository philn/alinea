# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AdminPage import AdminPage
from Alinea.Comments import AlineaComment
from quixote import get_request

class CommentDirectory(AdminPage):

    _actions = ['save','delete','back']
    _rolesRequired = {'delete':['admin',],
                      'save': ['admin'],
                      }

    def __init__(self, root):
        AdminPage.__init__(self,'Comments.admin.Comment')
        self.setRoot(root)
        
    def actions(self):
        request = get_request()
        commentID = request.get_field('id')
        self.commentID = commentID
        return AdminPage.actions(self)

    def _q_lookup(self, component):

        try:
            component = int(component)
        except:
            raise ValueError('No comment ?')
        else:
            self.commentID = component
        return self
    
    def title(self):
        return 'Comment !'

    def getComment(self, uid=None):
        if uid is None:
            uid = self.commentID
        return AlineaComment.Alinea_getCommentWithID(uid)

    def save(self, request):
        """ update the section w/ the current value from the request """
        uid = int(self.requestValue('id'))
        c = self.getComment(uid)
        c.author  = self.requestValue('author')
        c.email   = self.requestValue('email')
        c.url     = self.requestValue('url')
        c.comment = self.requestValue('comment')
        return self.back(request)

    def delete(self, request):
        comment = self.getComment()
        publicID = comment.publicID
        path = comment.path()
        
        if comment:
            AlineaComment.AlineaComment.delete(comment.id)

        # refresh caches
        self.refreshPageCache('Comment', 'Comment_content', publicID)
        for obj in path:
            objId = obj.publicID
            try:
                objType = obj.getTypeAsString()
            except:
                continue
            self.refreshPageCache(objType, '%s_content' % objType, objId)
            
        return self.back(request)

    def back(self, request):
        return request.response.redirect(self.getCommentsURL())
