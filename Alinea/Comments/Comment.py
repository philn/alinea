# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.SitePage import SitePage
from Alinea.Comments.AlineaComment import Alinea_getCommentWithID, AlineaComment
from Alinea.Articles.AlineaArticle import Alinea_getArticleWithID
from Alinea.AlineaLib import Tools, ReST
from Alinea.AlineaLib.AlineaTools import VirtualRow
from Alinea.AlineaLib.AlineaNode import AlineaNode
from quixote.errors import AccessError
import mx.DateTime.ISO
import time
from Alinea.Articles.AlineaArticle import Alinea_getArticleWithID
from Alinea.Comments.AlineaComment import Alinea_getCommentWithID

publisher = ReST.Publisher()

class Comment(SitePage):

    def __init__(self, commentID=None):
        SitePage.__init__(self,'Comments.Comment')
        self.setCommentID(commentID)

    def setCommentID(self, commentID):
        self.commentID = commentID

    def getCommentID(self):
        return self.commentID

    def getObject(self):
        try:
            comment = self._comment
        except AttributeError:
            self._comment = Alinea_getCommentWithID(self.getCommentID())
            comment = self._comment
        return comment

    def objectID(self):
        return 'Comment_%s' % self.getCommentID()

    def title(self):
        comment = self.getObject()
        return comment.title_or_id()

class NewComment(SitePage):
    """ AddComment Servlet.

        Also provides comment display/post support:
         - preview
         - editing comment (not saved) stored in session
    """
    
    _actions = SitePage._actions + [ 'preview', 'postComment' ]

    def __init__(self, objType=None, objID=None):
        self.setObjectType(objType)
        self.setObjectID(objID)
        SitePage.__init__(self,'Comments.AddComment')

    def setObjectType(self, objType):
        self.objType = objType

    def getObjectType(self):
        return self.objType

    def setObjectID(self, objID):
        self.objID = objID

    def getObjectID(self):
        return self.objID

    def getObject(self):
        funcMap = {'Article': Alinea_getArticleWithID,
                   'Comment': Alinea_getCommentWithID
                  }
        func = funcMap.get(self.getObjectType())
        if func:
            obj = func(self.getObjectID())
        else:
            obj = None
        return obj
    
    ##########################################################
    # Storing / Retrieving EditingComment from Session
    ##########################################################

    def getEditingComment(self):
        comment = self.getFromSession('editingComment')
        if not comment:
            comment = VirtualRow(AlineaComment)
	    comment.format = 'ReST'
	    comment.html = ''
        self.setEditingComment(comment)
        return comment

    def setEditingComment(self, comment):
        self.storeToSession('editingComment', comment)

    def delEditingComment(self):
        self.delFromSession('editingComment')

    ##########################################################
    # EditingComment Preview stuff
    ##########################################################

    def getHtmlPreview(self):
        " lazy get accessor on preview data "
        result = self.getFromSession('htmlPreview')
        if result is None:
            result = ''
        self.setHtmlPreview('')
        return result

    def setHtmlPreview(self, html):
        " html preview modifier "
        self.storeToSession('htmlPreview', html)


    ##########################################################
    # Comment post obscurated ID
    ##########################################################

    def getObscuratedID(self):
        result = self.getFromSession('obscuratedID')
        if result is not None:
            result = str(result)
        return result

    def setObscuratedID(self):
        import random
        i = random.randint(0,2000000)
        self.storeToSession('obscuratedID',i)

    ##########################################################
    # Actions
    ##########################################################

    def preview(self,request):
        " action executed when pressing preview button "
        print 'Preview ...'
        self.postComment(request,False)
        comment = self.getEditingComment()
        if comment.html:
            self.setHtmlPreview(comment.html)
        url = "%s/Comments/add_on_%s_%s/" % (self.config().site_url,
                                             self.getObjectType(),
                                             self.getObjectID())
        print url
        return request.response.redirect(url)

    def postComment(self,request,forReal=True):
        " comment saving for real or not (preview) "

        # spam protection
        if self.requestValue('obscuratedID',0) != self.getObscuratedID() :
            # if the id isn't the same as in session someone try fuck me
            # we should place a better Error message here ?
            print "SPAM Protection:"
            print " request value: %s" % self.requestValue('obscuratedID',0)
            print " cookie  value: %s" % self.getObscuratedID()

            self.setMessage("You are not allowed to post here since you don't seems to try to hack / or don't accept cookies")
            self.delEditingComment()
            return self.redirect()

        comment = self.getEditingComment()
        comment.url = self.requestValue('url')
        comment.author  = self.requestValue('name')
        comment.email = self.requestValue('email')
        comment.format = self.requestValue('format')
        comment.ip = self.request.environ['REMOTE_ADDR']

        data  = self.requestValue('comment')
        comment.comment = data
        date = time.localtime()[0:6]
        comment.date = mx.DateTime.ISO.Date(*date)
        warnings = ''

        if comment.format == 'Raw':
            # cosmetic processings on raw data
            data  = Tools.beautyfullText( data )
        elif comment.format == 'ReST':
            # ReST publishing
            data  = publisher.publish(data)
            warnings = publisher.getWarnings()

        commentable = self.getObject()
        
        if warnings:
            self.setMessage('ReST Error:'+warnings)
            comment.html = ''
        else:
            if not commentable.allowComments():
                return self.redirect()
            
            # XXX: big security hole here ? Tag Injection
            comment.html = data
            if forReal:
                comment.comment = comment.html
                # deletion of non available fields in table schema
                del comment['html']
                del comment['format']
                
                comment = comment.insertAndGetRow()                
                rgt = AlineaNode.openHole(commentable)
                comment.set(lft=rgt, rgt=rgt+1)
                AlineaNode.clearCache()
                
                self.delEditingComment()
            else:
                self.setEditingComment(comment)

        if forReal:
            self.setHtmlPreview('')
            self.delEditingComment()
            
            self.refreshPageCache('Welcome','Welcome_content')
            print '>>> REFRESH <<<'
            articleId = None
            for obj in comment.path():
                objId = obj.publicID
                try:
                    objType = obj.getTypeAsString()
                except:
                    continue
                if objType == 'Article':
                    articleId = objId
                self.refreshPageCache(objType, '%s_content' % objType, objId)

            if articleId:
                # TODO: refresh cache on Feeds
                #pass
                #import pdb; pdb.set_trace()
                self.refreshPageCache('Article','comment_content', articleId)
                
            print '>>> REFRESH <<<'
            
            # TODO: depending on whatever redirect to anywhere else
            return self.redirect()

    def formats(self):
        " available editing formats "
        return ['ReST','Raw']

    def redirect(self):
        obj = self.getObject()
        return self.request.response.redirect(obj.public_url())
