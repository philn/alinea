# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from sqlobject import *
import re
from Alinea.config import site_admin_url, site_url
from Alinea.AlineaLib.Tools import CustomHTMLParser
from Alinea.AlineaLib.AlineaTools import getObjectWithClassAndID
from Alinea.AlineaLib.AlineaNode import AlineaNode, Alinea_getNodeWithID

##########################################################
### Alinea Comment
##########################################################

class AlineaComment(AlineaNode):
    _inheritable = False
    publicID = IntCol(alternateID=True, default=None)
    author         = StringCol(length = 128)
    email          = StringCol(length=128)
    url            = StringCol(length=128)
    ip             = StringCol(length=64)
    comment        = StringCol()
    date           = DateTimeCol()

    def _create(self, id, **kw):
        publicIds = [c.publicID for c in AlineaComment.select()]
        if publicIds:
            if not kw.has_key('publicID') or not kw['publicID']:
                publicID = max(publicIds)
                kw['publicID'] = publicID + 1
        else:
            kw['publicID'] = 1
        return AlineaNode._create(self, id, **kw)
    
    def strip(self):
        parser = CustomHTMLParser()
        data = self.comment
        if data == None:
            data = ""
        parser.feed(data)
        return parser.getUsefullData()

    def getTypeAsString(self):
        return 'Comment'

    def allowComments(self):
	return False
    
    def getHtml(self):
        return self.comment

    def title_or_id(self):
        from Alinea.Articles.AlineaArticle import AlineaArticle
        parent = self.getParent()
        parentTitle = parent.title_or_id()
        title = AlineaNode.title_or_id(self)
        
        try:
            parentParent = parent.getParent()
            match = re.match("(.*)\son\s(.*)", parentParent.title_or_id())
        except AttributeError:
            parentParent = None
            match = None
        
        if isinstance(parent, AlineaComment):
            if isinstance(parentParent, AlineaComment) and match:
                author = match.groups()[0]
                title = "%s replies to %s" % (self.author, author)
            else:
                title = "%s replies to %s" % (self.author, parentTitle)
        elif isinstance(parent, AlineaArticle):
            title = "%s on %s" % (self.author, parentTitle)
        return title

    def admin_url(self):
        """ return the article URL in admin site view """
        return site_url + '/Comments/admin/Comment/%s/' % self.publicID

    def public_url(self):
        return site_url + '/Comments/%s/' % self.publicID

    def getComments(self):
        comments = self.getChildren(self.__class__)
        comments = list(comments)
        return comments

    def search(cls, query):
        match = "'%%%s%%'" % query
        comments = cls.select("alinea_comment.comment LIKE %s" % match)
        try:
            comments = list(comments)
        except:
            comments = []
        return comments

    search = classmethod(search)

AlineaComment.sqlmeta.cacheValues = True
    
def Alinea_getCommentWithID(id):
    """ return the comment w/ a given ID or None """
    comments = list(AlineaComment.select(AlineaComment.q.publicID == id))
    comment = comments[0]
    return comment
