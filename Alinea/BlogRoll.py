


from SitePage import SitePage
import os


class BlogRollPage(SitePage):

    def __init__(self):
        SitePage.__init__(self,'BlogRoll')

    def getBlogRoll(self):
        try:
            bloglinesLogin = self.config().bloglinesLogin
        except AttributeError:
            return ''
        
        if bloglinesLogin == '':
            return ''
        blogrollCache = os.path.join('/tmp','bloglines.%s.tmp' % bloglinesLogin)
        blogrollURL = 'http://rpc.bloglines.com/blogroll?html=1&id=%s' % bloglinesLogin
        return self._getEntriesFromFileCache(blogrollCache, blogrollURL)

    def title(self):
        return 'BlogRoll'
