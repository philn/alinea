# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

import sys
sys.path.append('..')

from sqlobject import *

from Alinea.config import site_admin_url, site_url
from Alinea.Articles.AlineaArticle import AlineaArticle
from Alinea.AlineaLib.AlineaNode import AlineaNode, Alinea_getNodeWithID

##########################################################
### Alinea Section
##########################################################

class AlineaSection(AlineaNode):
    _inheritable = False
    name = StringCol(length = 128)
    publicID = IntCol(alternateID=True)
    
    def _create(self, id, **kw):
        publicIds = [ a.publicID for a in AlineaSection.select()]
        if publicIds:
            if not kw.has_key('publicID') or not kw['publicID']:
                publicID = max(publicIds)
                kw['publicID'] = publicID + 1
        else:
            kw['publicID'] = 1
        return AlineaNode._create(self, id, **kw)

    def getArticles(self,orderBy=AlineaArticle.q.date):
        articles = self.getChildren(AlineaArticle)
        if type(orderBy) == str and orderBy == "title":
            orderBy = AlineaArticle.q.title
        articles = articles.orderBy(orderBy)
        articles = list(articles)
        return articles

    def getSubSections(self):
        return self.getChildren(AlineaSection)

    def getDirectSubSections(self):
        sections = self.getSubSections()
        return [ section for section in sections if section.getParent() == self ]

    def _insert(self, child, tree, depth):
        """ TODO: move that to AlineaNode """
        parent, children = tree
        item = [child,[]]
        if not children:
            tree[1] = [item]
        else:
            i = 0
            while i < depth:
                children = children[-1][-1]
                i += 1
            children.append(item)
        return tree

    def asTree(self):
        """ TODO: move that to AlineaNode (?) """        
        tree = [ self, [] ]
        children = self.getSubSections()
        right = []
        for child in children:
            if len(right):
                while right and right[len(right)-1] < child.rgt:
                    right = right[:-1]
            tree = self._insert(child,tree, len(right))
            right.append(child.rgt)
        return tree

    def admin_url(self):
        """ return the full url used for editing session """
        return site_url + '/Sections/admin/Section/%s/' % self.id

    def public_url(self):
        """ return the full url for a section """
        return site_url + '/Sections/%s/' % self.id

    def name_or_id(self):
        if len(self.name) > 0:
            return self.name
        return str(self.id)
    
    def getArticleWithTitle(self,value):
        "return the first article w/ the name in this section"
        articles = AlineaArticle.select( AND (AlineaArticle.q.title == value,
                                              AlineaArticle.q.alinea_sectionID == self.publicID) )
        try:
            return articles[0]
        except IndexError:
            return None

AlineaSection.sqlmeta.cacheValues = True

def Alinea_getRootSection():
    return AlineaSection.byPublicID(0)

def Alinea_getSectionWithID(id):
    return Alinea_getNodeWithID(id)

def Alinea_delSection(section):
    " cascade delete section articles "
    AlineaSection.delete(id=section.id)
