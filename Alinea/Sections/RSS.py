# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$


from Alinea.RSS import RSS 
from Alinea.Articles.RSS import ArticlesRSSPage
from Alinea.Sections.AlineaSection import Alinea_getSectionWithID
from sqlobject.sqlbuilder import AND

class SectionRSSPage(ArticlesRSSPage):

    def __init__(self,sectionID):
        ArticlesRSSPage.__init__(self)
        self.sectionID = sectionID

    def getArticles(self,orderBy='date'):
        " return the last 10 articles "
        section = Alinea_getSectionWithID(self.sectionID)
        articles = section.getArticles()
        articles.reverse()
        return articles[:10]

    def getFeedTitle(self):
        title = RSS.getFeedTitle(self)
        section = Alinea_getSectionWithID(self.sectionID)
        return "%s / %s" % (title, section.name)

    def getFeedUrl(self):
        section = Alinea_getSectionWithID(self.sectionID)
        return section.public_url()
