# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.CustomBatch import CustomBatch
from Alinea.Sections.AlineaSection import *


class SectionsPage(CustomBatch):

    _template = 'Sections.admin.Sections'
    _location = '/Sections/admin/'

    DEFAULT_RANGE = 5
    SERVLET = 'Sections'

    def title(self):
        return 'Admin Sections'

    def getSize(self):
        rootSection = Alinea_getRootSection()
        return len(rootSection.getDirectSubSections())

    def getFolds(self):
        folds = self.get('folds')
        if not folds:
            folds = {}
            for section in AlineaSection.select():
                folds[section.id] = True
            self.store('folds',folds)
        return folds

    def _fold(self, request, sectionID):
        self.awakePage(request)
        sectionID = int(sectionID)
        self.getFolds()[sectionID] = True
        return self.redirect(request,self._location)

    def _unfold(self, request, sectionID):
        self.awakePage(request)
        sectionID = int(sectionID)
        self.getFolds()[sectionID] = False
        return self.redirect(request,self._location)

    def isFolded(self, sectionID):
        try:
            result = self.getFolds()[int(sectionID)]
        except KeyError:
            self._fold(self.request,sectionID)
            result = True
        return result

    def getSections(self):
        """ Return the list of sections """
        begin = self.getBegin()
        end = begin + self.getRange()
        orderBy = self.getOrderBy()
        reverse = self.getReverse()
        return self.getRootSection().getDirectSubSections()

    
    
    def orders(self):
        return ['name','id']

    def ranges(self):
        return [ self.DEFAULT_RANGE, 10, 15, 20]

    def handleFoldingActions(self):
        action = self.requestValue('action')
        if action:
            sectionID = self.requestValue('sectionID')
            if sectionID:
                if action == 'fold':
                    self.fold(sectionID)
                elif action == 'unFold':
                    self.unFold(sectionID)

    def render(self):
        self.handleFoldingActions()
        return CustomBatch.render(self)
