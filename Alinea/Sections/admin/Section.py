# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Sections import SectionsPage
from Alinea.Sections import AlineaSection
from Alinea.AlineaLib.AlineaTools import *
from Alinea.AlineaLib.AlineaNode import AlineaNode
from types import StringType
import random,re, sys
from Alinea.AlineaLib.Directory import Directory
from quixote import get_request

newInRE = re.compile('new_in_(\d+)')

class FoldDirectory(Directory):

    def __init__(self, func):
        self.func = func

    def _q_lookup(self, component):
        return self.func(component)

########################################################################
### Internal functions
########################################################################


def _getPage(component):
    request = get_request()
    ref = request.environ['HTTP_REFERER']
    print request.dump()
    fromSections = ref.endswith('/admin/')
    print "fromSections: %s (%s)" % (fromSections, ref)
    try:
        int(ref[-1])
        fromSection = True
    except:
        fromSection = False
    v = SectionDirectory()
    if fromSections:
        v = SectionsPage()
    elif fromSection:
        v.init(int(ref[-1]))
    else:
        v.init(int(component))
    return v

def _unfold(component):
    request = get_request()
    v = _getPage(component)
    return v._unfold(request, component)

def _fold(component):
    request = get_request()
    v = _getPage(component)
    return v._fold(request, component)


class SectionDirectory(SectionsPage):
    """ Editing a given Section.
        Inheritance from `Sections` is needed if we want
        to display the sub-sections-tree of the editing Section

    """

    _template = 'Sections.admin.Section'
    _actions = ['save','delete','back'] 

    def init(self, sectionID, subSectionID=None):
        self.sectionID = sectionID
        self.subSectionID = subSectionID
        self.firstEdit = True
        self._location = '/Sections/admin/Section/%s' % self.sectionID


    _q_exports = ['', 'actions', 'unfold', 'fold'] 


    def _q_lookup(self, sectionID):
        match = newInRE.match(sectionID)
        subSectionID = None
        if sectionID == '':
            raise ValueError, 'No Section ?'
        elif sectionID == 'new':
            sectionID = 'new%s' % random.randint(0,6000)
        elif match:
            subSectionID = match.groups()[0]
            sectionID = 'new%s' % random.randint(0,6000)
        self.init(sectionID, subSectionID=subSectionID)
        return self


    ########################################################################
    ### Exported functions
    ########################################################################

    def actions(self):
        request = get_request()
        sectionID = request.get_field('id')
        self.init(sectionID)
        return SectionsPage.actions(self)


    fold = FoldDirectory(_fold)
    unfold = FoldDirectory(_unfold)


    def render(self):
        if self.isNew():
            section = self.newSection()
            if self.firstEdit:
                self.setMessage('Section created')
                self.firstEdit = False
        else:
            section = AlineaSection.Alinea_getSectionWithID(self.sectionID)
            if self.firstEdit:
                self.setMessage('Editing Section "%s"' % section.name_or_id())
                self.firstEdit = False
        self.storeSection(section)
        return SectionsPage.render(self)
    
    def title(self):
        if self.isNew():
            title = "new section"
        else:
            title = 'Section "%s"' % self.getSectionOrError().name
        return title

    def isNew(self):
        """ Currently editing a new section ?
        """
        return type(self.sectionID) != int and self.sectionID.startswith('new')

    def newSection(self):
        id = self.sectionID
        parentID = self.subSectionID
        section = VirtualRow(AlineaSection.AlineaSection)
        section.id = self.sectionID
        section.rgt = 0
        section.lft = 0
        
        if parentID is not None:
            section.parentID = int(parentID)
        else:
            section.parentID = None
        print 'new section: %s' % section
        return section

    def getSectionOrError(self):
        """ First try to get `section` from Session.
            If fetch fails, we look for `sectionID` and fetch
            AlineaSection identified by it in DB
        """
        sections = self.getFromSession('editingSections',{})
        section = sections.get(self.sectionID)
        if not section:
            section = AlineaSection.Alinea_getSectionWithID(self.sectionID)
        return section

    def getSectionAsList(self):
        """ Format the Section to be able to display it
            as a tree using displaySections macro
        """
        section = self.getSectionOrError()
        if section and not isinstance(section,VirtualRow):
            return section.getSubSections()
        else:
            return []

    def storeSection(self, section):
        if not isinstance(section,VirtualRow):
            return
        sections = self.getFromSession('editingSections', {})
        sections[section.id] = section
        self.storeToSession('editingSections', sections)


    def save(self, request):
        """ update the section w/ the current value from the request """
        section = self.getSectionOrError()
        if not section:
            self.renderPage()

        parent = self.requestValue('parent')
        if parent == '' or section.id == int(parent):
            parent = None
        else:
            parent = AlineaSection.Alinea_getSectionWithID(parent)

        if isinstance(section,VirtualRow):
            del section['parentID']
            section = section.insertAndGetRow()
            rgt = AlineaNode.openHole(parent)
            section.set(lft=rgt, rgt=rgt+1)
            AlineaNode.clearCache()
            self.delFromSession('editingSections')
                                
        elif section.getParent().publicID != parent.publicID:
            section.moveTo(parent)
            
        section.name = self.requestValue('name')
        
        self.setMessage("Section '%s' saved" % section.name)
        return self.back(request)

    def delete(self, request):
        section = self.getSectionOrError()
        name = section.name
        try:
            AlineaSection.Alinea_delSection(section)
            self.setMessage("Deleted section:'%s'" % name)
        except Exception,e:
            self.setMessage("Error when deleting section: '%s'" % str(e))
        return self.back(request)

    def back(self, request):
        return request.response.redirect(self.getSectionsURL())
