# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.SitePage import SitePage
from Alinea.Sections.AlineaSection import *
from quixote import get_request

class SectionPage(SitePage):
    """ Section Servlet.

    """

    _q_exports = SitePage._q_exports + [('rss.xml','indexRSS'),
                                        ('rss2.xml', 'indexRSS2'),
                                        ('atom.xml','indexAtom')]


    def __init__(self, sectionID=None):
        SitePage.__init__(self, 'Sections.Section')
        self.setSectionID(sectionID)

    def setSectionID(self, sectionID):
        self.sectionID = sectionID

    def getSectionID(self):
        return self.sectionID

    def title(self):
        return "%s / %s" % (self.config().site_name,self.getSection().name)
    
    def objectID(self):
        return 'Section_%s' % self.getSectionID()
    
    def getSection(self):
        " Alinea Section retrieving "
        return Alinea_getSectionWithID( self.getSectionID() )

    def indexRSS(self):
        from RSS import SectionRSSPage
        return SectionRSSPage(self.getSectionID()).handle(get_request())

    def indexRSS2(self):
        from RSS2 import SectionRSS2Page
        return SectionRSS2Page(self.getSectionID()).handle(get_request())

    def indexAtom(self):
        from Atom import SectionAtomPage
        return SectionAtomPage(self.getSectionID()).handle(get_request())
