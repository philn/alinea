# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from AlineaLib import FSCheetah
from RSS import RSS
import time

class RSS2(RSS):

    def __init__(self,template):
        RSS.__init__(self, template)
        self._master = FSCheetah.FSCheetah('templates.RSS2Page')

    def formatDate(self, aDate):
        aDate = time.mktime(aDate.timetuple())
        format = '%Y-%m-%dT%H:%M:%SZ'
        return time.strftime(format, time.gmtime(aDate))
