#!/usr/bin/env python
# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

"""
Here is a sample Apache config:

########################################################################

# map our Alinea instance running on localhost:8080 to /
<Location "/">
    SCGIServer localhost:8080
    SCGIHandler On
</Location>

# let Apache serve static files
# you need a /var/www/static which should be a symbolic link
# targetting /some/path/to/alinea/Alinea/static
<Location "/static">
    SCGIHandler Off
</Location>   

########################################################################
"""

from quixote.server.scgi_server import run
from quixote.publish import Publisher
from quixote.config import Config
from quixote.session import SessionManager
from Alinea.AlineaLib.Session import MySession, SessionPickle
from Alinea.config import site_url, sessionsPickleFile
import urllib2, threading

def serve_forever(port, alineaPackage, configFile):

    config = Config()
    config.read_file(configFile)

    sessionPickle = SessionPickle(sessionsPickleFile)
    sessions = sessionPickle.loadSessions()
    sessMgr = SessionManager(session_class=MySession, session_mapping=sessions)

    def create_publisher():        

        directory = __import__('%s' % alineaPackage).Directory.AlineaDirectory()
        pub = Publisher(directory, config=config, session_manager=sessMgr)
        return pub

    # we have to give SCGI the script_name of you app, so we extract it
    # from the site url.
    script_name = urllib2.urlparse.urlparse(site_url)[2]

    lock = threading.Lock()

    try:
        run(create_publisher, port=port,script_name=script_name,max_children=1)
    except KeyboardInterrupt:
        if not lock.locked:
            sessionPickle.saveSessions(sessMgr.sessions)
            lock.acquire()

if __name__ == '__main__':
    serve_forever(sys.argv[1], 'Alinea', 'server.conf')
