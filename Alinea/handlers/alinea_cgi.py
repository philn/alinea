#!/usr/bin/python
import cgitb; cgitb.enable()

# Quixote
from quixote.directory import Directory
from quixote.config import Config
from quixote import __version__
from quixote.publish import Publisher
from quixote.session import SessionManager
from quixote.http_request import HTTPRequest

from Alinea.AlineaLib.Session import MySession, SessionPickle
from Alinea import config
from Alinea.Directory import AlineaDirectory

import cgi, sys, os

CONFIG = '/home/phil/devel/alinea/server.conf'

class EmptyFile:

    def write(self, data):
        pass

def main():

    out = sys.stdout
    sys.stdout = EmptyFile()
    sys.stderr = EmptyFile()

    # session handling
    sessionPickle = SessionPickle(config.sessionsPickleFile)
    sessions = sessionPickle.loadSessions()
    sessMgr = SessionManager(session_class=MySession, session_mapping=sessions)

    directory = AlineaDirectory()
    conf = Config()
    conf.read_file(CONFIG)
    publisher = Publisher(directory, session_manager=sessMgr,config=conf)

    request = HTTPRequest(sys.stdin, os.environ)
    response = publisher.process_request(request)

    response.write(out)

    sessionPickle.saveSessions(sessMgr.sessions)

if __name__ == '__main__':
    main()
