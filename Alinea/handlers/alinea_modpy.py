# -*- coding: iso-8859-1 -*-
# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

"""
Here is a sample Apache2 config:

########################################################################
<LocationMatch "/alinea(/)?(.*)$">
    SetHandler python-program
    PythonPath "['/home/phil/devel','/home/phil/devel/alinea']+sys.path"
    PythonHandler quixote.server.mod_python_handler
    PythonOption quixote-publisher-factory alinea_modpy.create_publisher
    PythonOption alinea-config-file /home/phil/devel/alinea/tc2.conf
    PythonInterpreter Alinea.phil
</LocationMatch>

RewriteEngine On

# Directly serve static files & favicon
RewriteCond %{REQUEST_FILENAME} !^/(/)?static(.*)$
RewriteCond %{REQUEST_FILENAME} !^/favicon.ico$

# Prevent from url infinite loop
RewriteCond %{REQUEST_FILENAME} !^/alinea(.*)$

# Map / to /alinea/ 
RewriteRule ^/(/)?(.*) /alinea/$2 [L,PT,QSA]

########################################################################
"""


_publisher = None

def create_publisher(opts):
    global _publisher

    if _publisher is None:

        from quixote.server.mod_python_handler import ModPythonPublisher
        from quixote.config import Config
        from Alinea.AlineaLib.Session import MySession
        from Alinea.AlineaLib.ModPySessionManager import ModPySessionManager
        from Alinea.Directory import AlineaDirectory
        
        sess_manager = ModPySessionManager(session_class=MySession)
        
        configFile = opts['alinea-config-file']
        config = Config()
        config.read_file(configFile)
        
        _publisher = ModPythonPublisher(AlineaDirectory(), 
                                        config=config,
                                        session_manager=sess_manager)

    publisher = _publisher
    return publisher
