#!/usr/bin/env python
# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$


# Medusa quixote server: use to debug
#

# general
import sys

# Alinea config
try:
    from Alinea import config
except ImportError,e:
    print
    print e
    print """\

Please configure Alinea by:
- copying Alinea/config.py.template to Alinea/config.py
- editing Alinea/config.py to suit your needs

Now i exit ... Bye bye.
"""
    sys.exit(1)


# Quixote
from quixote import __version__
from quixote.publish import Publisher
from quixote.session import SessionManager
# medusa
import asyncore

from quixote.server.medusa_server import QuixoteHandler
from medusa import http_server

from Alinea.AlineaLib.Session import MySession, SessionPickle

################################################################################
def serve_forever(port, alineaPackage, configFile):

    print 'Quixote %s is starting Alinea on port %d' % (__version__,port)
    server = http_server.http_server('', port)

    # session handling
    sessionPickle = SessionPickle(config.sessionsPickleFile)
    sessions = sessionPickle.loadSessions()
    sessMgr = SessionManager(session_class=MySession, session_mapping=sessions)

    from quixote.directory import Directory
    from quixote.config import Config
    directory = __import__('%s' % alineaPackage).Directory.AlineaDirectory()
    conf = Config()
    conf.read_file(configFile)
    publisher = Publisher(directory,
                          session_manager=sessMgr,config=conf)
    dh = QuixoteHandler(publisher, server)
    server.install_handler(dh)
    
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        sessionPickle.saveSessions(sessMgr.sessions)

if __name__ == '__main__':
    serve_forever(sys.argv[1], 'Alinea', 'server.conf')
