# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from AdminPage import AdminPage
from Users.AlineaUser import AlineaUser, Alinea_doLogin

class LoginPage(AdminPage):
    """ Login Servlet """

    _actions = ['login']

    def __init__(self):
        AdminPage.__init__(self, 'Login')
        
    def awakePage(self, request):
        self.request = request
        self.setMessage('Access restricted ... you need to login')
        self.login()

    def title(self):
        return 'Alinea / Login'

    def login(self):
        login  = self.requestValue('login')
        password = self.requestValue('password')

        if login and password:
            user = Alinea_doLogin(login, password)
            sessID = self.getSessionID()
            if not sessID:
                self.setMessage('Problem with your session')
            elif user:
                user.session = sessID
                self.setMessage('User logged in..')
                
                referer = self.getFromSession('referer')
                if not referer:
                    self.request.response.redirect(self.config().site_admin_url)
                else:
                    self.storeToSession('referer','')
                    self.request.response.redirect(referer)
            else:
                self.setMessage('Wrong login / password')
