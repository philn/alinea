# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AlineaLib.Directory import Directory
from quixote import get_request
from quixote.errors import AccessError

from admin import ArticlesAdminDirectory
from archives import ArchivesDirectory

from Article import ArticlePage
from RSS import ArticlesRSSPage
from Atom import ArticlesAtomPage
from RSS2 import ArticlesRSS2Page
import sys

class ArticlesDirectory(Directory):
    _q_exports = [('rss.xml','indexRSS'), ('atom.xml', 'indexAtom'),
                  ('rss2.xml','indexRSS2'), 'admin','archives']

    def __init__(self, root):
        Directory.__init__(self,root)
        self.admin = ArticlesAdminDirectory(root)
        self.archives = ArchivesDirectory(root)
        
    def _q_lookup(self, component):
        try:
            articleID = int(component)
        except:
            return """\
This shall be the Articles index page.
Please supply an Article ID as last part of the url"""
        page = self.getPage('Article', ArticlePage)
        page.setArticleID(articleID)
        return page

    def indexRSS(self):
        page = self.getPage('ArticlesRSS', ArticlesRSSPage)
        request = get_request()
        return page.handle(request)

    def indexRSS2(self):
        page = self.getPage('ArticlesRSS2', ArticlesRSS2Page)
        return page.handle(get_request())

    def indexAtom(self):
        page = self.getPage('ArticlesAtom', ArticlesAtomPage)
        return page.handle(get_request())

    def addArticle(self,login, password, title, data, sectionID, format='ReST'):
        from Alinea.Users.AlineaUser import Alinea_doLogin
        from Alinea.Articles.AlineaArticle import Alinea_addArticle

        user = Alinea_doLogin(login, password)
        if not user:
            raise AccessError("Unknown user / password")

        article = Alinea_addArticle(title=title, format=format, data=data,
                                    published=False, alineaSectionID=sectionID,
                                    alineaUser=user)
        return True
