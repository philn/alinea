# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.CustomBatch import CustomBatch
from Alinea.Articles.AlineaArticle import AlineaArticle
from Alinea.Users.AlineaUser import AlineaUser

class ArticlesPage(CustomBatch):

    _template = 'Articles.admin.Articles'
    _location = '/Articles/admin/'

    DEFAULT_ORDERBY= 'date'
    DEFAULT_RANGE=10
    REVERSE=True
    SERVLET = 'Articles'
    _q_exports = [ 'actions' ]

    def getSize(self):
        query = AlineaArticle.select(self.getClause())
        size = query.count()
        return size

    def title(self):
        return 'Articles admin'

    def ranges(self):
        " display range list "
        return [ 5, self.DEFAULT_RANGE, 20, 30, 40 ]

    def orders(self):
        " orderBy list"
        return ['date','id','title','alinea_user_id','published',
                'format']

    def getArticles(self):
        """ Return the list of articles """
        begin = self.getBegin()
        end = begin + self.getRange()
        orderBy = self.getOrderBy()
        reverse = self.getReverse()
        articles = list(AlineaArticle.select(self.getClause(), orderBy=AlineaArticle.q.date,
                                             reversed=reverse)[begin:end] )
        return articles

    def getClause(self):
        userID = self.getUserID()
        if not userID or userID == '-1':
            result = 'all'
        else:
            result = 'alinea_user_id = %s' % userID
        return result

    def getUserID(self):
        return self.get('userID')

    def setUserID(self, user):
        self.store('userID', user)

    def users(self):
        return AlineaUser.select()

    def render(self):
        previousUserID = self.requestValue('id')
        userID = self.requestValue('userID',None)
        if previousUserID != userID:
            self.setUserID(userID)
        return CustomBatch.render(self)
