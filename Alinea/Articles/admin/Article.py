# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.AdminPage import AdminPage
import time, datetime
import random, re
import textwrap
from Alinea.Articles.AlineaArticle import *
from Alinea.AlineaLib import Tools, ReST
from Alinea.AlineaLib.AlineaTools import VirtualRow
from Alinea.AlineaLib.AlineaNode import AlineaNode

from quixote import get_request

publisher = ReST.Publisher()
newInRE = re.compile('new_in_(\d+)')


class ArticleDirectory(AdminPage):

    _actions = ['fileupload','save','delete','back','preview',
                'publish','unpublish']
    _rolesRequired = {'save': ['author'], 'publish':['publisher'],
                      'unpublish': ['publisher'] }

    
    def __init__(self):
        AdminPage.__init__(self,'Articles.admin.Article')

    def init(self, articleID, sectionID=None):
        if not articleID.startswith('new'):
            self.articleID = int(articleID)
        else:
            self.articleID = articleID
        self.firstEdit = True
        self.sectionID = sectionID

    def _q_lookup(self, articleID):
        request = get_request()
        formArticleID = request.get_field('id')
        if formArticleID:
            articleID = formArticleID
            
        match = newInRE.match(articleID)
        sectionID = None
        if articleID == '':
            raise ValueError, 'No Article ?'
        elif articleID == 'new':
            articleID = 'new%s' % random.randint(0,6000)
        elif match:
            articleID = 'new%s' % random.randint(0,6000)
            sectionID = match.groups()[0]
        self.init(articleID, sectionID)
        return self

    def title(self):
        return self.getArticle().title

    def render(self):
	article = None
        if self.isNew():
            self.setSaved(False)
            if self.firstEdit and not self.getArticle():
                article = self.newArticle()
                self.setMessage('Article created')
                self.firstEdit = False
        else:
            article = Alinea_getArticleWithID(self.articleID)
            self.setSaved(True)
            if self.firstEdit:
                self.setMessage('Editing Article "%s"' % article.title_or_id())
                self.firstEdit = False
        if article:
            self.storeArticle(article)
        return AdminPage.render(self)

    ## Articles/Session   ###################################
    #########################################################

    def newArticle(self):
        """ Create a skeleton for a new Article.

            Return a VirtualRow acting as an AlineaArticle
        """
        date = datetime.datetime.now()
        userID = self.getLoggedUser().id
        article = VirtualRow(AlineaArticle)
        article.date = date
        article.lastModified = date
        article.alineaUserID = userID
        article.id = self.articleID
        article.publicID = self.articleID
        article.published = False
        article.alineaComments = []
        article.title = 'New Article'
        article.alineaSection = None
        article.alineaSectionID = None
        article.hits = 0
        if self.sectionID is not None:
            article.alineaSectionID = int(self.sectionID)
        return article

    def isNew(self):
        """ Currently editing a new article ?
        """
        result = type(self.articleID) != int and self.articleID.startswith('new')
        return result

    def storeArticle(self, article):
        """ Put an Article in safe place on session.

        We use a hash to store the article mapped by article id. We
        can't store AlineaArticles since they are not pickable. So we
        store only VirtualRows.
        """
        if not isinstance(article,VirtualRow):
            return
        articles = self.getFromSession('editingArticles', {})
        articles[article.id] = article
        self.storeToSession('editingArticles', articles)

    def getArticle(self, id=None):
        """ Fetch an Article into Session.

            If the article is not in Session, it is in db.
        """
        if id is None:
            id = self.articleID
        if not self.isNew():
            id = int(id)
        articles = self.getFromSession('editingArticles',{})
        try:
            article = articles[id]
        except KeyError:
            try:
                article = Alinea_getArticleWithID(self.articleID)
            except:
                article = None
        return article

    def delArticleFromSession(self, article):
        """ editing articles home-cleanup.

            Drop an article from the editingArticles hash.
        """
        articles = self.getFromSession('editingArticles',{})
        try:
            del articles[article.id]
        finally:
            self.storeToSession('editingArticles', articles)

    def setSaved(self, val):
        self.storeToSession('saved',val)

    def isSaved(self):
        return self.getFromSession('saved',False)

    ## Preview Stuff ########################################
    #########################################################

    def getHtmlPreview(self):
        " lazy get accessor on preview data "
        htmlPreview = self.getFromSession('htmlPreview')
        self.setHtmlPreview('')
        if htmlPreview is None:
            htmlPreview = self.getHtmlPreview()
        return htmlPreview

    def setHtmlPreview(self, html):
        " html preview modifier "
        self.storeToSession('htmlPreview', html)

    def preview(self, request):
        result = self.save(request,False)
        article = self.getArticle()
        if result and article is not None:
            self.setHtmlPreview(article.html)
        return self.render()

    #########################################################
    #########################################################

    ## Actions       ########################################
    #########################################################

    def save(self, request, forReal=True):
        """ update the article w/ the current value from the request """
        article = self.getArticle()

        article.title = self.requestValue('title')
        article.data = self.requestValue('data')
        article.format = f = self.requestValue('format')
        article.language = self.requestValue('language')
        
        warnings = ''
        ok = True

        if f == 'Raw':
            # cosmetic processings on raw data
            article.html  = Tools.beautyfullText( article.data )
        elif f == 'ReST':
            # ReST publishing
            article.html  = publisher.publish(article.data)
            warnings = publisher.getWarnings()
        else:
            # HTML data
            # XXX: big security hole here ? Tag Injection
            article.html = article.data

        if warnings != '':
            self.setMessage("Error: %s" % warnings)
            ok = False
        
        if forReal and ok:
            section = None

            # enable section stuff only if saving article for real
            if self.requestValue('section'):
                from Alinea.Sections.AlineaSection import AlineaSection

                sectionId = int(self.requestValue('section'))
                section = AlineaSection.get(sectionId)
                if not isinstance(article, VirtualRow) and article.getParent().publicID != section.publicID:
                    article.moveTo(section)

            if section is None:
                raise ValueError("You need to put the Article in a Section ...")

            if isinstance(article, VirtualRow):
                del article['alineaComments']
                del article['publicID']
                del article['alineaSection']
                del article['alineaSectionID']

                self.delArticleFromSession(article)
                article.alineaUser = self.getLoggedUser()
                article = article.insertAndGetRow()

                rgt = AlineaNode.openHole(section)
                article.set(lft=rgt, rgt=rgt+1)
                AlineaNode.clearCache()

            article.published = False
            article.touch()
            self.setSaved(True)
            self.setMessage("Article '%s' saved" % article.title)
            self.storeArticle(article)
                
        if forReal:
            if ok:
                #self.refreshPageCache('Welcome',
                return request.response.redirect(article.admin_url())
            else:
                return self.render()
        else:
            self.storeArticle(article)
            return ok

    def delete(self, request):
        article = self.getArticle()
        if not self.isNew():
            if article.author().login == self.getLoggedUser().login:
                name = article.title
                self.refreshRelatedCaches()
                Alinea_delArticle(article)
            else:
                raise AccessError("You can't delete articles you didn't wrote")
        else:
            name = ''
        self.setMessage("Deleted article:'%s'" % name)
        return self.back(request)

    def back(self, request):
        return request.response.redirect(self.getArticlesURL())

    def fileupload(self, request):
        upload = self.requestValue('filename')
        
        contents = ''.join(upload.readline())
        article = self.getArticle()
        article.data  = contents
        article.html  = contents
        self.storeArticle(article)
        
        filename = self.requestValue('filename').orig_filename
        self.setMessage("Uploaded file: '%s'" % filename)
        
        return self.render()

    def publish(self, request):
        article = self.getArticle()
        article.published = True
        self.refreshRelatedCaches()
        return request.response.redirect(article.admin_url())

    def unpublish(self, request):
        article = self.getArticle()
        article.published = False
        self.refreshRelatedCaches()
        return request.response.redirect(article.admin_url())

    #########################################################
    #########################################################

    ## Helpers       ########################################
    #########################################################

    def formats(self):
        " available editing formats "
        return ['ReST', 'Raw', 'HTML']

    def refreshRelatedCaches(self):
        article = self.getArticle()
        date = article.date.strftime('%d/%m/%Y')
        self.refreshPageCache('Welcome','Welcome_content')
        self.refreshPageCache('ArticlesRSS', 'RSS_content')
        self.refreshPageCache('ArticlesRSS', 'RSS_itemLinks')
        self.refreshPageCache('ArticlesRSS2', 'RSS_content')        
        self.refreshPageCache('ArticlesAtom', 'Atom_content')
        self.refreshPageCache('Archive', 'archives_content', date)
        
        # TODO:
        # - refresh Article's section page cache and feeds
        # - refresh Article's author page cache
        
    #########################################################
    #########################################################
