# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.SitePage import SitePage
from Alinea.Articles.AlineaArticle import Alinea_getArticlesForDay
import datetime

class ArchivesPage(SitePage):

    def __init__(self):
        SitePage.__init__(self, 'Articles.archives.Archives')

    def title(self):
        return 'Archives'

    def setDateStr(self, dateStr):
        self.date = dateStr

    def _getDateAsList(self):
        return [int(c) for c in self.date.split('_')]

    def getArticles(self):
        year, month, day = self._getDateAsList()
        articles = Alinea_getArticlesForDay(year, month, day)
        return articles

    def getDate(self):
        year, month, day = self._getDateAsList()
        return datetime.date(year, month, day).strftime('%d/%m/%Y')
