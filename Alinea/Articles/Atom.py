# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.Atom import Atom
from Alinea.Articles.AlineaArticle import AlineaArticle
import datetime

class ArticlesAtomPage(Atom):

    def __init__(self):
        Atom.__init__(self,'Articles.Atom')

    def getArticles(self,orderBy='date'):
        " return the last 10 articles "
        articles = list(AlineaArticle.select(AlineaArticle.q.published==True,
                                             orderBy=orderBy, reversed=True)[0:10] )
        return articles

    def getLastModifiedDate(self):
        try:
            lastArticle = AlineaArticle.select(AlineaArticle.q.published==True,
                                                    orderBy='-date')[:1]
            lastArticle = lastArticle[0]            
        except IndexError:
            return datetime.datetime.now()
        else:
            return lastArticle.lastModified
            
    def getFeedTitle(self):
        title = Atom.getFeedTitle(self)
        return "%s / Articles" % title
