# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.RSS import RSS
from Alinea.Articles.AlineaArticle import AlineaArticle

class ArticlesRSSPage(RSS):

    def __init__(self):
        RSS.__init__(self,'Articles.RSS')

    def getArticles(self,orderBy='date'):
        " return the last 10 articles "
        articles = list(AlineaArticle.select(AlineaArticle.q.published==True,
                                             orderBy=orderBy, reversed=True)[0:10] )
        return articles

    def getItemsLinks(self):
	return [ article.public_url() for article in self.getArticles() ]

    def getFeedTitle(self):
        title = RSS.getFeedTitle(self)
        return "%s / Articles" % title
