# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from sqlobject import *
from sqlobject import sqlbuilder
from Alinea.config import site_admin_url, site_url
from Alinea.Users.AlineaUser import Alinea_getUserWithID
from Alinea.AlineaLib.AlineaNode import AlineaNode
import datetime, calendar, re

__all__ = ['AlineaArticle', 'Alinea_getArticleWithID', 'Alinea_delArticle']

##########################################################
### Alinea Article
##########################################################
class AlineaArticle(AlineaNode):
    _inheritable = False
    publicID = IntCol(alternateID=True)
    title     = StringCol(length = 100)
    format    = StringCol(length = 64)
    data      = StringCol()
    html      = StringCol()
    date      = DateTimeCol(default=datetime.datetime.now)
    lastModified = DateTimeCol(default=datetime.datetime.now)
    hits      = IntCol(default = 0)
    published = BoolCol(default= False)
    alineaUser = ForeignKey('AlineaUser')
    language = StringCol()
    
    def _create(self, id, **kw):
        publicIds = [ a.publicID for a in AlineaArticle.select()]
        if publicIds:
            if not kw.has_key('publicID') or not kw['publicID']:
                publicID = max(publicIds)
                kw['publicID'] = publicID + 1
        else:
            kw['publicID'] = 1
        return AlineaNode._create(self, id, **kw)

    def allowComments(self):
        now = datetime.datetime.now()
        written_on = self.date
        delta = now - written_on
        limit = 7
        return delta.days < limit

    def getComments(self):
        from Alinea.Comments.AlineaComment import AlineaComment
        comments = self.getChildren(AlineaComment)
	comments = comments.orderBy(AlineaComment.q.date)
        comments = list(comments)
        return comments

    def getCommentsNb(self):
        return self.getChildrenNb()

    def public_url(self):
        """ return the full url for an article """
        return site_url + '/Articles/%s/' % self.publicID

    def admin_url(self):
        """ return the article URL in admin site view """
        return site_url + '/Articles/admin/Article/%s/' % self.publicID

    def getTypeAsString(self):
        return 'Article'

    def getHtml(self):
        return self.html

    def author(self):
        """ return the article author as an AlineaUser """
        return self.alineaUser

    def title_or_id(self):
        if len(self.title) > 0:
            return self.title
        return str(self.publicID)

    def getHits(self):
        return self.hits

    def incrHits(cls, articleID):
        #self.hits += 1
        table = cls.sqlmeta.table        
        cls.doQuery(sqlbuilder.Update(table, {'hits': cls.q.hits +1},
                                      where=AND(cls.q.publicID == articleID)))
        
    incrHits = classmethod(incrHits)

    def touch(self):
        self.lastModified = datetime.datetime.now()
        # tmp hack...
        self.date = datetime.datetime.now()
        
    def search(cls, query):
        match = "'%%%s%%'" % query
        articles = cls.select("""alinea_article.data LIKE %s
                                 OR alinea_article.title LIKE %s""" % (match, match))
        try:
            articles = list(articles)
        except:
            articles = []
        return articles
    
    search = classmethod(search)

AlineaArticle.sqlmeta.cacheValues = True

def Alinea_getArticleWithID(id):
    article = AlineaArticle.byPublicID(id)
    return article

def Alinea_delArticle(article):
    " cascade delete article comments and delete the article itself "
    AlineaArticle.delete(article.id)

def Alinea_addArticle(title, format, data, published, alineaSectionID,
                      alineaUser):
    from Alinea.Sections.AlineaSection import Alinea_getSectionWithID
    from Alinea.AlineaLib import Tools, ReST
    
    section = Alinea_getSectionWithID(alineaSectionID)
    warnings = ""
    publisher = ReST.Publisher()
    
    if format == 'Raw':
        # cosmetic processings on raw data
        html  = Tools.beautyfullText(data)
    elif format == 'ReST':
        # ReST publishing
        html  = publisher.publish(data)
        warnings = publisher.getWarnings()
    else:
        # HTML data
        html = data

    if warnings:
        raise ValueError(warnings)
    
    article = AlineaArticle(title=title, format=format, data=data,
                            html=html, #alineaSection=section,
                            alineaUser=alineaUser)
    rgt = AlineaNode.openHole(section)
    article.set(lft=rgt, rgt=rgt+1)

    
    userRoles = [ r.label for r in alineaUser.roles ]
    if 'publisher' in userRoles and published:
        article.published = True
    return article

def Alinea_getArticlesForDay(year, month, day):
    articleTime = datetime.datetime(year, month, day)
    date = AlineaArticle.q.date
    published = AlineaArticle.q.published
    after = articleTime + datetime.timedelta(hours=23,
                                             minutes=59, seconds=59)
    articles = list(AlineaArticle.select(AND(date >= articleTime,
                                             date < after,
                                             published == True)))
    return articles

def Alinea_getArticlesForMonth(year, month):
    dummy, daysNumber = calendar.monthrange(year, month)
    firstDay = datetime.datetime(year, month, 1)
    lastDay = datetime.datetime(year, month, daysNumber)
    lastDay = lastDay + datetime.timedelta(hours=23,minutes=59,seconds=59)

    date = AlineaArticle.q.date
    published = AlineaArticle.q.published
    articles = AlineaArticle.select(AND(date >= firstDay, date < lastDay,
                                        published == True))
    articlesByDay = {}
    for day in range(1,daysNumber+1):
        articlesByDay[day] = 0
    for article in list(articles):
        day = article.date.day
        articlesByDay[day] += 1
    return articlesByDay
