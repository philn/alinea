# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from Alinea.SitePage import SitePage
from Alinea.Articles.AlineaArticle import *
from Alinea.Comments.RSS import CommentsRSSPage
from Alinea.Comments.RSS2 import CommentsRSS2Page
from Alinea.Comments.Atom import CommentsAtomPage
from Alinea.Comments.Comment import NewComment
import time
from quixote import get_request

class ArticlePage(SitePage):
    """ Article Servlet.

        Used to render individual articles.
        Also provides comment display/post support:
         - preview
         - editing comment (not saved) stored in session
    """

    _q_exports = SitePage._q_exports + [('rss.xml','commentsRSS'),
                                        ('rss2.xml', 'commentsRSS2'),
                                        ('atom.xml', 'commentsAtom'),
                                        'source']
    _actions = SitePage._actions + [ 'preview', 'postComment' ]

    def __init__(self, articleID=None):
        SitePage.__init__(self,'Articles.Article')
        self.setArticleID(articleID)

    def awakePage(self,request):
        SitePage.awakePage(self, request)
        self.comment.request = request
        
    def objectID(self):
        return 'Article_%s' % self.getArticleID()

    ##########################################################
    # Misc
    ##########################################################

    def setArticleID(self, articleID):
        self.articleID = articleID
        if articleID:
            page = self.getPage('NewComment', NewComment)
            page.setObjectType('Article')
            page.setObjectID(articleID)
            self.comment = page
            AlineaArticle.incrHits(articleID)

    def __getattr__(self, name):
        return getattr(self.comment, name)
            
    def getArticleID(self):
        return self.articleID

    def getArticle(self):
        result = Alinea_getArticleWithID( self.getArticleID() )
        return result
    
    getObject = getArticle
    
    def commentsRSS(self):
        request = get_request()
        return ArticleCommentsRSS(self.getArticleID()).handle(request)

    def commentsRSS2(self):
        request = get_request()
        return ArticleCommentsRSS2(self.getArticleID()).handle(request)

    def commentsAtom(self):
        request = get_request()
        return ArticleCommentsAtom(self.getArticleID()).handle(request)

    def source(self):
        # XXX: make some beautiful HTML here ?
        article = self.getArticle()
        request = get_request()
        request.response.set_content_type('text/plain')
        return article.data

    ###
    ### New Comment
    ###

    def preview(self, request):
        self.comment.preview(request)
        return request.response.redirect(self.getArticle().public_url())
    


class ArticleFeed:

    def __init__(self, articleID):
        self.articleID = articleID

    def _getArticle(self):
        try:
            article = self._article
        except AttributeError:
            self._article = Alinea_getArticleWithID(self.articleID)
            article = self._article
        return article

    def getFeedUrl(self):
        article = self._getArticle()
        return article.public_url()

    def getFeedTitle(self):
        article = self._getArticle()
        return 'Comments on article "%s"' % article.title_or_id()

    def getFeedDesc(self):
        article = self._getArticle()
        return article.getEncodedHtml()

    def getComments(self):
        " return the last 10 comments "
        article = self._getArticle()
        comments = article.getComments()
	comments.reverse()
	return comments

class ArticleCommentsRSS(ArticleFeed,CommentsRSSPage):

    def __init__(self, articleID):
        CommentsRSSPage.__init__(self)
        ArticleFeed.__init__(self, articleID)

class ArticleCommentsRSS2(ArticleFeed,CommentsRSS2Page):

    def __init__(self, articleID):
        CommentsRSS2Page.__init__(self)
        ArticleFeed.__init__(self, articleID)

class ArticleCommentsAtom(ArticleFeed,CommentsAtomPage):

    def __init__(self, articleID):
        CommentsAtomPage.__init__(self)
        ArticleFeed.__init__(self, articleID)
