import os

BASEDIR = os.path.dirname(__file__)

def get_config_filename():
    import os
    try:
        name = os.environ['ALINEA_CONFIG']
    except KeyError:
        raise RuntimeError, "You need to set ALINEA_CONFIG env variable point to your config file"
    return name

filename = get_config_filename()

if not os.path.exists(filename):
    import sys
    print "'%s' file not found. Create one using config_default.py.template" % filename
    print "Then set the ALINEA_CONFIG env variable to point to that file"
    print "and give Alinea another chance :)"
    sys.exit()

# backward compatibility
audioScrobblerLogin = ''
gvisitHash = ''

# Load the user config file
execfile(filename)

# Things that should never change whatever the config used
default_templates_dir = BASEDIR + '/templates'

# Home-cleaning
del os
del filename
