# Copyright (C) 2004-05 Alinea Team
#
# This file is part of Alinea (http://pythonfr.org/alinea/)
#
# Alinea is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# Alinea is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alinea; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# See AUTHORS file for informations about developpers and contributors.
# $Id$

from quixote.errors import AccessError
from quixote import get_request
from SitePage import SitePage
from AlineaLib import FSCheetah
from config import *

class AdminPage(SitePage):

    _actions = ['logout']
    _rolesRequired = {}

    def __init__(self, name):
        self._adminMaster = FSCheetah.FSCheetah('templates.AdminPage')
        SitePage.__init__(self, name)

    def awakePage(self,request):
        """ we are setting the request """
	redir = SitePage.awakePage(self, request)
        user = self.getLoggedUser()
        if not user:
            referer = self.config().site_url + request.get_environ('REQUEST_URI')
            #referer = request.get_environ('REQUEST_URI')
            self.storeToSession('referer',referer)
	    url = self.config().site_url + '/login'
            return request.response.redirect(url)
	return redir

    def actions(self):
        user = self.getLoggedUser()
	request = get_request()
        if not user:
	    url = self.config().site_url + '/login'
	    return request.response.redirect(url)
	else:
	    return SitePage.actions(self)
	
    def checkRole(self, action):
        try:
            roles = self._rolesRequired[action]
        except KeyError:
            return True
        else:
            userRoles = [ r.label for r in self.getLoggedUser().roles ]
            for roleName in self._rolesRequired[action]:
                if roleName not in userRoles:
                    raise AccessError("You need role '%s' to access this page" % roleName)
            return True

    def render(self):
        #hack to permit the reload of the master template
        # we first check the admin page template
        if FSCheetah.FSCHEETAH_AUTO_RELOAD:
            if self._adminMaster.changed():
                if FSCheetah.FSCHEETAH_AUTO_COMPILE:
                    self._adminMaster.build()
                reload(self._adminMaster.module())
                reload(self._module)
                self._instance = None
        return SitePage.render(self)

    def getLoggedUser(self):
        from Users.AlineaUser import AlineaUser
        ret = None
        sessID = self.getSessionID()
        if sessID is not None:
            result = AlineaUser.select(AlineaUser.q.session == sessID)
            if result.count() > 0:
                ret = result[0]

        return ret

    ##################################################################
    ### URLS
    ##################################################################

    def getAdminURL(self):
        return self.config().site_admin_url

    def getRolesURL(self):
        return self.config().site_url + '/Users/admin/Roles/'

    def getUsersURL(self):
        return self.config().site_url + '/Users/admin/'

    def getArticlesURL(self):
        return self.config().site_url + '/Articles/admin/'

    def getSectionsURL(self):
        return self.config().site_url + '/Sections/admin/'

    def getCommentsURL(self):
        return self.config().site_url + '/Comments/admin/'


    ##################################################################
    ### Tools
    ##################################################################
##     def getAllSections(self):
##         """ return all sections sorted by hierarchy for displaying"""
##         from Sections.AlineaSection import Alinea_getSectionsByHierarchyOrder, getAllRootAlineaSections
##         return Alinea_getSectionsByHierarchyOrder(getAllRootAlineaSections())

    getAllSections = SitePage.getSections
