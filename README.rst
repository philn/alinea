
Introduction to Alinea
=======================

Alinea is a blog tool designed to be modular at some extent. Basically
it manages some users who write some articles organized in
sections. Readers can react to articles by posting comments.

From the technical point of view, Alinea is written in Python_ with the
Quixote_ web framework, coupled with SQLObject_ for data management
and with Cheetah_ for templating.

.. _Python: http://python.org
.. _Quixote: http://quixote.ca
.. _SQLObject: http://sqlobject.org
.. _Cheetah: http://cheetahtemplate.org


Users don't have all the same abilities in Alinea, that'd be anarchy
then ;-) Each user is assigned a set of roles. For instance there's a
role "author" which makes users able to write articles. The
"publisher" role is used to make articles available to read in the
front page of Alinea.

Features
========

As of current Alinea's state, the features are the following:

- Components:

  * Users

  * Roles

  * Sections

    + hierarchic
    + RSS1/RSS2/Atom feeds of the section's articles

  * Articles

    + ReST support
    + preview
    + RSS1/RSS2/Atom feeds of the article's comments
    + RSS1/RSS2/Atom feeds of the latest articles published

  * Comments

    + threaded comments
    + available editing formats : Raw, ReST

- XHTML Cheetah templates (you can edit with WYSIWYG HTML editors)
- Flexible templates editing cycle : edit, refresh the page, the
  involved templates re-compile by them-selves
- XML-RPC API
- Calendar
- Customizable templates: you want Article 45 to be displayed
  differently ? create the template Articles/templates/Article_45.tmpl !
- Designs support : you can make Alinea look different via CSS +
  custom templates stored in Alinea/designs/ directory.
- Deployment strategies: 

  * medusa : for test/dev purpose
  * mod_python : production

- Databases supported : SQLite, MySQL, PostgreSQL, ... everything which is
  supported by SQLObject !
- Del.icio.us_ social links system support

.. _Del.icio.us: http://del.icio.us/


Developer notes
===============

Here is the Alinea directory mapping:

- Alinea/ : contains all the stuff
- Alinea/AlineaLib/ : place for the common Alinea 
- Alinea/static/ : static stuff directory (CSS, imgs, ...)
- Alinea/XXXX/ : place for the XXXX product
- Alinea/XXXX/__init__.py : file defining exported symbols of the
  product (Directory, urls, see below)
- Alinea/XXXX/templates : place for the XXXX product's templates
- Alinea/XXXX/admin : place for the XXXX product admin section
- Alinea/XXXX/admin/templates : guess by yourself :)

From the design point of view, an Alinea directory can be seen as a
set of:

- a Model : some SQLObject classes
- a View : some Cheetah templates held in ``templates/`` directories
- a Controller : a SitePage inheriting from ``Alinea.SitePage.SitePage``

Though the Model can be used by more than one View/Controller (ex:
Article and admin/Article). 

Each product shall provide an ``install.py`` script containing code
like this one:

::

  from Alinea.Users.AlineaUser import *

  def setup():
      AlineaUser.dropTable(ifExists=True)
      AlineaUser.createTable()
      # ...

  if __name__ == '__main__':
     setup()

When  you add a new product, don't forget to add its name in the
``Alinea/packages.py`` file. From the Quixote's point of view each
product is a Directory you need to define:

::

  from Alinea.AlineaLib.Directory import Directory

  class ArticlesDirectory(Directory):
      _q_exports = [('rss.xml','indexRSS'), 'admin']

      admin = ArticlesAdminDirectory()

      def _q_lookup(self, component):
          try:
              articleID = int(component)
          except:
              return """\
  This shall be the Articles index page.
  Please supply an Article ID as last part of the url"""
          from Article import View
          view = View(articleID)
          return view

      def indexRSS(self):
          from RSS import View
          request = get_request()
          return View().handle(request)

The Directory is then created by the main Alinea directory (see
``Alinea/__init__.py`` for more information).
